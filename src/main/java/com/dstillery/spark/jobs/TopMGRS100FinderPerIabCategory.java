package com.dstillery.spark.jobs;

import static com.dstillery.spark.common.Util.convertListToRDD;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.RelationalGroupedDataset;
import org.apache.spark.sql.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.spark.common.SparkConfig;
import com.dstillery.spark.jobs.geoanalysis.MGRSDetail;
import com.dstillery.spark.jobs.geoanalysis.TopMGRS100MySqlRow;
import com.dstillery.spark.jobs.geoanalysis.geojson.MGRStoGeoJsonConverter;
import com.dstillery.spark.jobs.geoanalysis.geojson.MGRStoGeoJsonConverter.GeoFeatureType;
import com.fasterxml.jackson.core.JsonProcessingException;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
/*
    finds the topMGrs100 palces for each iab category and writes it to redis
    UI uses this info in GeoAnalysis Controller
 */
public class TopMGRS100FinderPerIabCategory {

    private final static Logger LOGGER = LoggerFactory.getLogger(TopMGRS100FinderPerIabCategory.class);
    private static final int IAB_CAT_INDEX = 0;
    private static final int MGR100_INDEX = 2;
    //good documentation on how to read data from cassandra :
    //https://github.com/datastax/spark-cassandra-connector/blob/master/doc/14_data_frames.md
    //https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/sql/JavaSparkSQLExample.java
    private final SparkConfig sparkConfig;
    private final JedisPool jedisPool;
    private MGRStoGeoJsonConverter converter = new MGRStoGeoJsonConverter();

    public TopMGRS100FinderPerIabCategory() {
        sparkConfig = new SparkConfig();
        jedisPool = new JedisPool(new JedisPoolConfig(), "localhost");

    }

    private void saveIabCatsToTopMGRSInLastNHours(int hoursAgo) throws JsonProcessingException {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods_mdl", "featuretofeaturehistory");
        long lastNHours = Instant.now().minus(hoursAgo, ChronoUnit.HOURS).toEpochMilli();
        LOGGER.info("finding top data for : lastNHours : {}", lastNHours);
        Set<String> allIabCategoriesRecorded =
                dataSet.select(
                        "eventtime",
                        "featurekey",
                        "featurekeytype",
                        "featuretype")
                        .where("featurekeytype='IAB_CAT'")
                        .where("featuretype='MGRS100'")
                        .filter((FilterFunction<Row>) row -> row.getTimestamp(0).getTime() >= lastNHours)
                        .toJavaRDD().collect()
                        .stream().map(row -> row.getString(1))
                        .collect(Collectors.toSet());

        LOGGER.info("allIabCategoriesRecorded : {}", allIabCategoriesRecorded);


        RelationalGroupedDataset query = dataSet.select(
                "eventtime",
                "featurekey",
                "featurekeytype",
                "feature",
                "featuretype")
                .where("featurekeytype='IAB_CAT'")
                .where("featuretype='MGRS100'")
                .filter((FilterFunction<Row>) row -> row.getTimestamp(0).getTime() >= lastNHours)
                .groupBy("featurekey",
                        "featurekeytype",
                        "feature",
                        "featuretype");
        Dataset<Row> topMGRSDataSet = query.count();

        //How to get max of counts!!!
        //        Dataset<Row> maxOfCount = query.count().agg(max("count"));
//        LOGGER.info("maxOfCount");
//        Util.printDataSet(maxOfCount);


        //1000 mgrs when converted to GeoJson will be almost 2Mb
        List<Row> topMGRSForIabCats = topMGRSDataSet.toJavaRDD().takeOrdered(1000, new CountColumnComparator());

        try(Jedis jedis = jedisPool.getResource()) {
            for (String iabCat : allIabCategoriesRecorded) {
                createGeoJsonForIabCatAndCacheIt(topMGRSForIabCats, iabCat, jedis);
            }
        }

        LOGGER.info("end of saving data in redis");
    }

    private void createGeoJsonForIabCatAndCacheIt(
            List<Row> topMGRSForIabCats,
            String iabCat,
            Jedis jedis) throws JsonProcessingException {
        List<MGRSDetail> listOfMgrs = topMGRSForIabCats.stream()
                .filter(row -> {
                    return row.getString(IAB_CAT_INDEX).equalsIgnoreCase(iabCat);
                })
                .map(row -> new MGRSDetail(
                        row.getString(MGR100_INDEX),
                        row.getString(IAB_CAT_INDEX)
                )).collect(Collectors.toList());

        //these MGRS values are MGRS100 to we choose a 0.062138 mile for the radius of polygon to give us a 100m to 100m polygon
        String featureCollectionGeoJson = converter.convert(listOfMgrs, 0.062138, GeoFeatureType.POINT);

        String iabCatGeoFeatureCollectionKeyInRedis = "topMGRSForIabCats_last_24hr_"+iabCat;
        // we save this big json in redis on r3
        LOGGER.debug("setting cache entry with key : {}, featureCollectionGeoJson : {}", iabCatGeoFeatureCollectionKeyInRedis, featureCollectionGeoJson);
        jedis.set(iabCatGeoFeatureCollectionKeyInRedis, featureCollectionGeoJson);
        //1 day expiry
        jedis.expire(iabCatGeoFeatureCollectionKeyInRedis, 3600 * 24);
    }

    /**
     * this is kept here for later use. or demonstrating how query with sort works in spark
     * @param topMGRSForIabCats
     */
    private void saveTopMGRSToMySql(List<Row> topMGRSForIabCats) {
        List<TopMGRS100MySqlRow> mySqlRows = topMGRSForIabCats.stream()
                .map(row -> {

                    return new TopMGRS100MySqlRow(
                            row.getString(0),//featureKey as stated in group by clause
                            row.getString(1),//featureKeyType as stated in group by clause
                            row.getString(2),//feature as stated in group by clause
                            row.getString(3),//featuretype as stated in group by clause
                            Timestamp.from(Instant.now().truncatedTo(ChronoUnit.DAYS)),
                            row.getLong(4));//as added by count() to the row
                }).collect(Collectors.toList());

        JavaRDD<TopMGRS100MySqlRow> topMGRSForIabCatsRdd = convertListToRDD(sparkConfig, mySqlRows);

        LOGGER.info("numberOfPartitions : {}", topMGRSForIabCatsRdd.getNumPartitions());

        Dataset<Row> topMGRS100 = sparkConfig.getSqlContext()
                .createDataFrame(topMGRSForIabCatsRdd, TopMGRS100MySqlRow.class);

        String tableName = "iab_cat_to_top_mgrs100_last_24hr";
        sparkConfig.getMySqlDriver().overWrite(tableName, topMGRS100);
    }

    /**
     * this is just to show how to do select count(*), a, b from table group by a, b in spark
     */
    private void logGreaterThan10MGRS100PerIabCat() {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods_mdl", "featuretofeaturehistory");

        dataSet.select(
                        "eventtime",
                        "featurekey",
                        "featurekeytype",
                        "feature",
                        "featuretype"
                )
                .where("featurekeytype='IAB_CAT'")
                .where("featuretype='MGRS100'")
                .filter((FilterFunction<Row>) row -> row.getTimestamp(0).getTime() >= 24)
                .groupBy("featurekey",
                         "featurekeytype",
                         "feature",
                         "featuretype")
                .count()
                .where("count >= 1")
                .collectAsList().forEach(row -> {
                    System.out.println("row : " + row.mkString());
                    System.out.println("row schema : " + row.schema());
                });
    }

    public static void main(String[] args) throws Exception {

        TopMGRS100FinderPerIabCategory topMGRS100FinderPerIabCategory = new TopMGRS100FinderPerIabCategory();
        //for now we set it to 2400 hours, because we don't have enought data per day
        //TODO change it to 24 or 3h  when in real production
        topMGRS100FinderPerIabCategory.saveIabCatsToTopMGRSInLastNHours(72);
        topMGRS100FinderPerIabCategory.sparkConfig.getMetricReporterService()
                .addStateModuleForEntity(
                        "JOB_ENDED_WITH_SUCCESS",
                        topMGRS100FinderPerIabCategory.getClass().getSimpleName(),
                        "ALL");

        topMGRS100FinderPerIabCategory.jedisPool.close();
        topMGRS100FinderPerIabCategory.sparkConfig.stopSession();
    }
}

class CountColumnComparator implements Comparator<Row>, Serializable {
    @Override
    public int compare(Row o1, Row o2) {
        if (o1.getLong(4) == o2.getLong(4)) {
            return 0;
        }
        if (o1.getLong(4) > o2.getLong(4)) {
            return -1;
        }
        return 1;
    }
}
