package com.dstillery.spark.jobs.geoanalysis.geojson;

import java.awt.geom.Rectangle2D;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.simplify.TopologyPreservingSimplifier;
import com.vividsolutions.jts.util.GeometricShapeFactory;

public class GeometryUtil {
    private static final WKTReader wktReader = new WKTReader();

    public static Double MILES_PER_DEGREE = 69d;
    public static Double DEGREES_PER_MILE = (1/MILES_PER_DEGREE);
    public static Double TOLERANCE_MILES = 0.5d;
    public static Double DISTANCE_THRESHOLD_MILES = 0.00001d;

    public static Rectangle2D getRectangleFromPolygon(Polygon bounds) {
        Coordinate[] xy = bounds.getEnvelope().getCoordinates();
        Double minx = Math.min(Math.min(xy[0].x, xy[1].x), Math.min(xy[2].x, xy[3].x));
        Double miny = Math.min(Math.min(xy[0].y, xy[1].y), Math.min(xy[2].y, xy[3].y));
        Double maxx = Math.max(Math.max(xy[0].x, xy[1].x), Math.max(xy[2].x, xy[3].x));
        Double maxy = Math.max(Math.max(xy[0].y, xy[1].y), Math.max(xy[2].y, xy[3].y));
        Double w = Math.abs(maxx-minx);
        Double h = Math.abs(maxy-miny);
        Double x = minx;
        Double y = miny;
        Rectangle2D rect = new Rectangle2D.Double(x, y, w, h);
        return rect;
    }

    public static com.vividsolutions.jts.geom.Geometry getSimplifiedZipGeometry(Double latitude, String wktString)	throws ParseException {
        // we ensure that the closer we get to the poles the more forgiving the toleranceDegrees since we can't
        // set a separate tolerance on the latitude and longitude
        Double toleranceDegrees = GeometryUtil.convertLonMilesToDegrees(GeometryUtil.TOLERANCE_MILES, latitude);

        com.vividsolutions.jts.geom.Geometry rawZipGeometry = wktReader.read(wktString);
        TopologyPreservingSimplifier simplifier = new TopologyPreservingSimplifier(rawZipGeometry);
        simplifier.setDistanceTolerance(toleranceDegrees);
        com.vividsolutions.jts.geom.Geometry zipGeometry = simplifier.getResultGeometry();
        return zipGeometry;
    }

    public static List<Rectangle2D> sortByDistance(final Polygon poly, Iterable<Rectangle2D> matchList) {
        List<Rectangle2D> results = new ArrayList<>();

        for(Rectangle2D rect : matchList){
            results.add(rect);
        }

        Collections.sort(results, new Comparator<Rectangle2D>() {
            @Override
            public int compare(Rectangle2D rectA, Rectangle2D rectB) {
                BigDecimal latitudeA = BigDecimal.valueOf(rectA.getCenterY());
                BigDecimal longitudeA = BigDecimal.valueOf(rectA.getCenterX());
                BigDecimal latitudeB = BigDecimal.valueOf(rectB.getCenterY());
                BigDecimal longitudeB = BigDecimal.valueOf(rectB.getCenterX());

                Double distanceA = getDistanceFromPointToCentroid(latitudeA, longitudeA, poly.getCentroid());
                Double distanceB = getDistanceFromPointToCentroid(latitudeB, longitudeB, poly.getCentroid());

                return distanceA.compareTo(distanceB);
            }
        });

        return results;
    }

    public static Polygon getRectanglePolygonFromPointAndRadius(Double centerLatitude, Double centerLongitude, Double radiusInMiles){
        // ensure the searchbox is non zero (add a tiny bit)
        radiusInMiles = radiusInMiles + 0.00001f;

        Double diameterInMiles = 2 * radiusInMiles;
        Double lonWidthInDegrees = convertLonMilesToDegrees(diameterInMiles, centerLatitude);
        Double latHeightInDegrees = convertLatMilesToDegrees(diameterInMiles);
        Double leftEdgeLogitude = centerLongitude - (lonWidthInDegrees/2);
        Double topEdgeLatitude = centerLatitude - (latHeightInDegrees/2);
        Rectangle2D boundingBox = new Rectangle2D.Double(leftEdgeLogitude, topEdgeLatitude, lonWidthInDegrees, latHeightInDegrees);

        return getPolygonFromRectangle(boundingBox);
    }

    public static Polygon getRectanglePolygonFromCoordinates(Double xmin, Double ymin, Double xmax, Double ymax){
        Coordinate[] searchBoxCoordinates = {new Coordinate(xmin, ymin),new Coordinate(xmin, ymax),new Coordinate(xmax, ymax),new Coordinate(xmax, ymin),new Coordinate(xmin, ymin)};
        GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(PrecisionModel.FLOATING));
        LinearRing[] noHoles = {};
        CoordinateArraySequence searchBoxSequence = new CoordinateArraySequence(searchBoxCoordinates);
        Polygon box = new Polygon(new LinearRing(searchBoxSequence, geometryFactory), noHoles, geometryFactory);

        return box;
    }

    public static Polygon getPolygonFromRectangle(Rectangle2D rect){
        return getRectanglePolygonFromCoordinates(rect.getMinX(), rect.getMinY(), rect.getMaxX(), rect.getMaxY());
    }

    public static Double getDistanceFromPointToCentroid(BigDecimal lat, BigDecimal lon, Point centroid) {
        Double latA = lat.doubleValue();
        Double lonA = lon.doubleValue();
        Double latB = centroid.getY();
        Double lonB = centroid.getX();

        // we've got to convert to planar distances rather than degrees on a globe
        Double dxDegrees = Math.abs(lonA - lonB);
        Double dyDegrees = Math.abs(latA - latB);

        Double dyMiles =  convertLatDegreesToMiles(dyDegrees);
        Double dxMiles =  convertLonDegreesToMiles(dxDegrees, latA);

        Double distanceMiles = Math.sqrt(Math.pow(dxMiles, 2) + Math.pow(dyMiles, 2));

        // round to zero if it's really really small
        distanceMiles = (distanceMiles < DISTANCE_THRESHOLD_MILES) ? 0 : distanceMiles;

        return distanceMiles;
    }

    public static com.vividsolutions.jts.geom.Geometry parseKML(String wktStr) throws ParseException {
        wktStr =  wktStr.replaceAll(",", "*");
        wktStr =  wktStr.replaceAll(" ",",");
        wktStr =  wktStr.replaceAll("\\*"," ");
        wktStr =  wktStr.replaceAll("<coordinates>","((");
        wktStr =  wktStr.replaceAll("</coordinates>","))");
        wktStr =  wktStr.replaceAll("<.*?>","") + ")";
        wktStr =  wktStr.replaceAll("\\)\\)\\(\\(",")),((");

        wktStr = "MULTIPOLYGON(" + wktStr + ")";
        //System.out.println("wk:"+wktStr);

        com.vividsolutions.jts.geom.Geometry geometry = wktReader.read(wktStr);
        return geometry;
    }

    public static Polygon getCircleFromPoint(BigDecimal latitude, BigDecimal longitude, BigDecimal radiusMiles) {
        GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
        Coordinate coord = new Coordinate();
        coord.x = longitude.doubleValue();
        coord.y = latitude.doubleValue();
        shapeFactory.setCentre(coord);
        shapeFactory.setNumPoints(20);

        Double diameterMiles = 2 * radiusMiles.doubleValue();

        Double dyDegrees = convertLatMilesToDegrees(diameterMiles);
        Double dxDegrees = convertLonMilesToDegrees(diameterMiles, latitude.doubleValue());

        shapeFactory.setWidth(dxDegrees);
        shapeFactory.setHeight(dyDegrees);

        Polygon circle = shapeFactory.createCircle();

        return circle;
    }

    private static Double convertLatDegreesToMiles(Double deltaLatDegrees){
        Double deltaLatMiles =  deltaLatDegrees * MILES_PER_DEGREE;
        return deltaLatMiles;
    }

    private static Double convertLonDegreesToMiles(Double deltaLatDegrees, Double latitude){
        Double deltaLonMiles =  deltaLatDegrees * (MILES_PER_DEGREE * Math.cos(Math.toRadians(latitude)));
        return deltaLonMiles;
    }

    private static Double convertLatMilesToDegrees(Double deltaLatMiles){
        Double deltaLatDegrees = deltaLatMiles / MILES_PER_DEGREE;
        return deltaLatDegrees;
    }

    private static Double convertLonMilesToDegrees(Double deltaLonMiles, Double latitude){
        Double deltaLonDegrees = deltaLonMiles / (MILES_PER_DEGREE * Math.cos(Math.toRadians(latitude)));
        return deltaLonDegrees;
    }
}
