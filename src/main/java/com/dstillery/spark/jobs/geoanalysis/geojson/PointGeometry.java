package com.dstillery.spark.jobs.geoanalysis.geojson;

import java.util.ArrayList;
import java.util.List;

import shade.com.datastax.spark.connector.google.common.base.Objects;

public class PointGeometry implements Geometry {
    private String type = "Point";
    private List<Double> coordinates = new ArrayList<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Double> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PointGeometry)) return false;
        PointGeometry geometry = (PointGeometry) o;
        return Objects.equal(getType(), geometry.getType()) &&
                Objects.equal(getCoordinates(), geometry.getCoordinates());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getType(), getCoordinates());
    }
}
