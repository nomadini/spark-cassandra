package com.dstillery.spark.jobs.geoanalysis.geojson;

import shade.com.datastax.spark.connector.google.common.base.Objects;

import java.util.Map;

public class Feature {

    private String type = "Feature";

    private Map<String, Object> properties;

    private Geometry geometry;

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Feature)) return false;
        Feature feature = (Feature) o;
        return Objects.equal(getType(), feature.getType()) &&
                Objects.equal(getProperties(), feature.getProperties()) &&
                Objects.equal(getGeometry(), feature.getGeometry());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getType(), getProperties(), getGeometry());
    }
}
