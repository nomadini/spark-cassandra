package com.dstillery.spark.jobs.geoanalysis.geojson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dstillery.common.util.GUtil;
import com.dstillery.spark.jobs.geoanalysis.MGRSDetail;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shade.com.datastax.spark.connector.google.common.collect.ImmutableList;
import com.dstillery.common.geo.geocoordinates.geocoordinateconverter.GeoCoordinateConverter;
public class MGRStoGeoJsonConverter {
    public enum GeoFeatureType {
        POINT,
        POLYGON
    }
    private final static Logger LOGGER = LoggerFactory.getLogger(MGRStoGeoJsonConverter.class);

    private ObjectMapper objectMapper = new ObjectMapper();

    public String convert(List<MGRSDetail> mgrsValues,
                          double radiusOfPolygon,
                          GeoFeatureType geoFeatureType) throws JsonProcessingException {

        FeatureCollection fc = new FeatureCollection();
        List<Feature> features = new ArrayList<>();
        for (MGRSDetail mgrs : mgrsValues) {
            try {
                if (geoFeatureType.equals(GeoFeatureType.POLYGON)) {
                    //half a mile is good for MGR100
                    features.add(takePolygonGeoJsonFromMgrs(mgrs, 1, radiusOfPolygon));
                } else if (geoFeatureType.equals(GeoFeatureType.POINT)) {
                    features.add(takePointGeoJsonFromMgrs(mgrs, 1));
                }
            } catch (RuntimeException e) {
                if (GUtil.allowedToCall(1000)) {
                    LOGGER.warn("ignoring exception regarding bad data", e);
                }
            }
        }
        fc.setFeatures(features);
        return objectMapper.writeValueAsString(fc);
    }

    private static Feature takePointGeoJsonFromMgrs(MGRSDetail mgrs, int countOfVisit) {
        PointGeometry geometry = new PointGeometry();
        LOGGER.trace("mgrs.getMgrs100() : {}", mgrs.getMgrs100());
        double[] latLon = GeoCoordinateConverter.getInstance().mgrs2LatLon(mgrs.getMgrs100());
        LOGGER.trace("mgrs.getMgrs100() : {}, latLon: {}",mgrs.getMgrs100(),  Arrays.asList(latLon));

        geometry.setType("Point");
        //longitude shows up first for mapBox, thats why we put it in reverse order
        geometry.setCoordinates(ImmutableList.of(latLon[1], latLon[0]));

        Feature feature = new Feature();
        feature.setGeometry(geometry);
        Map map = new HashMap();
        map.put("countOfVisit", countOfVisit);
        map.put("iabCat", mgrs.getIabCat());
        map.put("mgrs100", mgrs.getMgrs100());
        feature.setProperties(map);
        feature.getProperties().put("hashCode",  mgrs.getIabCat() +"-" + feature.hashCode());
        return feature;
    }

    private static Feature takePolygonGeoJsonFromMgrs(MGRSDetail mgrs,
                                                      int countOfVisit,
                                                      double radiusOfPolygon) {
        List<List<List<Double>>> asd = new ArrayList<>();
        List<List<Double>> af = new ArrayList<>();

        PolygonGeometry geometry = new PolygonGeometry();

        LOGGER.trace("mgrs.getMgrs100() : {}", mgrs.getMgrs100());
        double[] latLon = GeoCoordinateConverter.getInstance().mgrs2LatLon(mgrs.getMgrs100());
        LOGGER.trace("mgrs.getMgrs100() : {}, latLon: {}",mgrs.getMgrs100(),  Arrays.asList(latLon));

        Polygon polygon = GeometryUtil.getRectanglePolygonFromPointAndRadius(
                latLon[0],
                latLon[1],
                radiusOfPolygon);


        Coordinate[] cors = polygon.getCoordinates();

        geometry.setType("Polygon");
        Arrays.asList(cors).forEach(cor -> {
           List<Double> ag = new ArrayList<>();
           ag.add(cor.x);
           ag.add(cor.y);

           af.add(ag);
       });

        asd.add(af);

        geometry.setCoordinates(asd);

        Feature feature = new Feature();
        feature.setGeometry(geometry);
        Map map = new HashMap();
        map.put("countOfVisit", countOfVisit);
        map.put("iabCat", mgrs.getIabCat());
        map.put("mgrs100", mgrs.getMgrs100());
        feature.setProperties(map);
        feature.getProperties().put("hashCode",  mgrs.getIabCat() +"-" + feature.hashCode());
        return feature;
    }

    public static void main(String args[]) throws JsonProcessingException {
        System.out.println(
                Collections.singletonList(new MGRStoGeoJsonConverter().convert(ImmutableList.of(
                        new MGRSDetail("19SDD845348", "IAB1"),
                        new MGRSDetail("19SDD845345", "IAB2")), 0.5, GeoFeatureType.POLYGON)));
    }
}
