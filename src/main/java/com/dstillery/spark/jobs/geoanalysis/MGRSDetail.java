package com.dstillery.spark.jobs.geoanalysis;

public class MGRSDetail {
    private String mgrs100;
    private String iabCat;

    public MGRSDetail(String mgrs100, String iabCat) {
        this.mgrs100 = mgrs100;
        this.iabCat = iabCat;
    }

    public String getMgrs100() {
        return mgrs100;
    }

    public void setMgrs100(String mgrs100) {
        this.mgrs100 = mgrs100;
    }

    public String getIabCat() {
        return iabCat;
    }

    public void setIabCat(String iabCat) {
        this.iabCat = iabCat;
    }
}
