package com.dstillery.spark.jobs.geoanalysis.geojson;

import java.util.ArrayList;
import java.util.List;


import shade.com.datastax.spark.connector.google.common.base.Objects;

public class PolygonGeometry implements Geometry {
    private String type = "polygon";
    private List<List<List<Double>>> coordinates = new ArrayList<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<List<List<Double>>> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<List<List<Double>>> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PolygonGeometry)) return false;
        PolygonGeometry geometry = (PolygonGeometry) o;
        return Objects.equal(getType(), geometry.getType()) &&
                Objects.equal(getCoordinates(), geometry.getCoordinates());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getType(), getCoordinates());
    }
}
