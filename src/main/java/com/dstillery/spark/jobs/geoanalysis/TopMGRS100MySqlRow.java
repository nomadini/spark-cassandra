package com.dstillery.spark.jobs.geoanalysis;

import java.io.Serializable;
import java.sql.Timestamp;
/*
this has to be a bean for spark to defer the struct type based on bean
 */
public class TopMGRS100MySqlRow implements Serializable {
    private String featureKey;
    private String featureKeyType;
    private String feature;
    private String featureType;
    private Long countOfVisit;
    private Timestamp createAt;

    public TopMGRS100MySqlRow(String featureKey,
                              String featureKeyType,
                              String feature,
                              String featureType,
                              Timestamp createAt,
                              Long countOfVisit) {
        this.featureKey = featureKey;
        this.featureKeyType = featureKeyType;
        this.feature = feature;
        this.featureType = featureType;
        this.countOfVisit = countOfVisit;
        this.createAt = createAt;
    }

    public Long getCountOfVisit() {
        return countOfVisit;
    }

    public void setCountOfVisit(Long countOfVisit) {
        this.countOfVisit = countOfVisit;
    }

    public String getFeatureKey() {
        return featureKey;
    }

    public void setFeatureKey(String featureKey) {
        this.featureKey = featureKey;
    }

    public String getFeatureKeyType() {
        return featureKeyType;
    }

    public void setFeatureKeyType(String featureKeyType) {
        this.featureKeyType = featureKeyType;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getFeatureType() {
        return featureType;
    }

    public void setFeatureType(String featureType) {
        this.featureType = featureType;
    }

    public Timestamp getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }
}