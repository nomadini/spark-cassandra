package com.dstillery.spark.jobs.geoanalysis.geojson;

import java.util.List;

public class GeoJsonData {
//    {
//        "type": "FeatureCollection",
//            "features": [
//        {
//            "type": "Feature",
//                "properties": {
//            "letter": "G",
//                    "color": "blue",
//                    "rank": "7",
//                    "ascii": "71"
//        },
//            "geometry": {
//            "type":"Polygon","coordinates":[[[-74.4486603463764,40.4994743933545],[-74.4486577412621,40.4994819756492],[-74.4486397749564,40.4994858009507],[-74.4485892896374,40.4995166765907],[-74.4484774493846,40.4995851904047],[-74.4484675679164,40.4995875812145],[-74.4484575966168,40.499586419964],[-74.4484494219477,40.4995815700354],[-74.4484450202028,40.499574465914],[-74.4484454693604,40.4995668153208],[-74.448450499926,40.4995599844334],[-74.448407650287,40.4995190674029],[-74.4484497812738,40.4994931783173],[-74.4485431162318,40.4994407853351],[-74.4485543451729,40.499436618486],[-74.448572221647,40.4994535591168],[-74.4485964761597,40.4994405804081],[-74.4485959371705,40.4994319734736],[-74.4486128254979,40.4994284214051],[-74.4486200120201,40.4994351156879],[-74.4486175865689,40.4994420148971],[-74.4486438173752,40.4994670842948],[-74.4486603463764,40.4994743933545]]]
//        }
//        }
//
//        ]
//    }

    private String type;
    private List<Feature> features;
    private String murmurHash;

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMurmurHash() {
        return murmurHash;
    }

    public void setMurmurHash(String murmurHash) {
        this.murmurHash = murmurHash;
    }

    @Override
    public String toString() {
        return "GeoJsonData{" +
                "type='" + type + '\'' +
                ", features=" + features +
                ", murmurHash='" + murmurHash + '\'' +
                '}';
    }
}
