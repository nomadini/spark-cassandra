package com.dstillery.spark.jobs.geoanalysis.geojson;

import java.util.List;

public class FeatureCollection {
    private String type = "FeatureCollection";

    private List<Feature> features;

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
