package com.dstillery.spark.jobs.models;

import java.io.Serializable;
import java.util.Date;

import org.spark_project.guava.base.Objects;

public class BadDevice implements Serializable {
    private Date time;
    private Long count;
    private String deviceId;

    public BadDevice(String deviceId, Long count, Date time) {
        this.count = count;
        this.deviceId = deviceId;
        this.time = time;
    }

    public Long getCount() {
        return count;
    }

    public Date getTime() {
        return time;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BadDevice)) {
            return false;
        }
        BadDevice badDevice = (BadDevice) o;
        return getTime() == badDevice.getTime() &&
                Objects.equal(getCount(), badDevice.getCount()) &&
                Objects.equal(getDeviceId(), badDevice.getDeviceId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getTime(), getCount(), getDeviceId());
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("time", time)
                .add("count", count)
                .add("deviceId", deviceId)
                .toString();
    }
}