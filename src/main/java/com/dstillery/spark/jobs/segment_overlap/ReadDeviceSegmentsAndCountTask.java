package com.dstillery.spark.jobs.segment_overlap;

import static com.dstillery.spark.common.Util.executeCassandraCommand;

import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import com.dstillery.common.cassandra.traits.DeviceSegmentHistoryCassandraServiceTypeTraits;
import com.dstillery.common.device.DeviceClassValue;
import com.dstillery.common.device.DeviceTypeValue;
import com.dstillery.common.device.NomadiniDevice;
import com.dstillery.common.modeling.devicesegment.DeviceSegmentHistory;
import com.dstillery.common.segment.Segment;
import com.dstillery.common.util.GUtil;

import jersey.repackaged.com.google.common.collect.ImmutableList;

public class ReadDeviceSegmentsAndCountTask implements Serializable, VoidFunction<Row> {

    private final static Logger LOGGER = LoggerFactory.getLogger(ReadDeviceSegmentsAndCountTask.class);

    private SparkConf sparkConf;
    private DeviceSegmentHistoryCassandraServiceTypeTraits deviceSegmentHistoryCassandraServiceTypeTraits =
            new DeviceSegmentHistoryCassandraServiceTypeTraits(null);

    private int sleepTime;
    private  Instant lookBack;
    private static AtomicLong numberOfRowsProcessed = new AtomicLong(0);
    private static AtomicLong numberOfCassandraCalls = new AtomicLong(0);
    private static Map<Instant, Long> dayCountMap = new ConcurrentHashMap<>();
    private static Map<List<Object>, Long> segmentTotalCountMap = new ConcurrentHashMap<>();
    private static Map<List<Object>, Long> segmentCompositionPerDay = new ConcurrentHashMap<>();

    public ReadDeviceSegmentsAndCountTask(int sleepTime,
                                          Instant lookBack,
                                          SparkConf sparkConf) {
        this.sleepTime = sleepTime;
        this.sparkConf = sparkConf;
        this.lookBack = lookBack;

    }

    public void call(Row originalRow) throws Exception {
        String deviceId = originalRow.getString(0);
        String deviceClass = originalRow.getString(1);
        if (GUtil.allowedToCall(1000)) {
            LOGGER.info("sleeping for {} millis", sleepTime);
        }
        Thread.sleep(sleepTime);

        try {

            NomadiniDevice device = new NomadiniDevice(deviceId, DeviceTypeValue.DISPLAY, DeviceClassValue.valueOf(deviceClass));
            LOGGER.trace("device : {} ", device);

            CassandraConnector connector = CassandraConnector.apply(sparkConf);
            Session session = connector.openSession();

            String sql = "select * from gicapods.devicesegments where deviceId = '" + device.getCompositeDeviceId() + "' and deviceType='" +
                    device.getDeviceClass() + "' and dateCreated >= " + lookBack.toEpochMilli();
            ResultSet result = executeCassandraCommand(session, sql);
            numberOfCassandraCalls.getAndIncrement();


            List<com.datastax.driver.core.Row> allRowsRead = result.all();
            List<DeviceSegmentHistory> deviceSegmentHistories = new ArrayList<>();
            for (com.datastax.driver.core.Row row : allRowsRead) {
                try {
                    numberOfRowsProcessed.getAndIncrement();
                    deviceSegmentHistoryCassandraServiceTypeTraits.extractRowData(row).ifPresent(deviceSegmentHistories::add);
                } catch (RuntimeException e) {
                    LOGGER.warn("suppressing exception", e);
                }
            }
            LOGGER.trace("sql : {}", sql);
            LOGGER.trace("deviceSegmentHistories : {}", deviceSegmentHistories.size());
            if (deviceSegmentHistories.size() > 1000) {
                LOGGER.warn("device with too long of deviceHistory: {} , history size : {}", device, deviceSegmentHistories.size());
            } else {
                process(deviceSegmentHistories);
            }
            session.close();

            if (GUtil.allowedToCall(1000)) {
                LOGGER.info("finished processing about 1000 devices");
            }

        } catch (Exception e) {
            LOGGER.warn("exception ", e);
        }
    }

    public void process(List<DeviceSegmentHistory> deviceSegmentHistories){

        getTimeToSegmentMap(deviceSegmentHistories).forEach((day, segmentSet) -> {

            if (dayCountMap.containsKey(day)) {
                dayCountMap.put(day, dayCountMap.get(day) + 1);
            } else {
                dayCountMap.put(day, 1L);
            }

            segmentSet.forEach(segment -> {
                List<Object> countSegmentsInDayKey = ImmutableList.of(day, segment.getId());
                if (segmentTotalCountMap.containsKey(countSegmentsInDayKey)) {
                    segmentTotalCountMap.put(countSegmentsInDayKey, segmentTotalCountMap.get(countSegmentsInDayKey) + 1);
                } else {
                    segmentTotalCountMap.put(countSegmentsInDayKey, 1L);
                }

                /*
                    ATTENTION : it's very important to have overlap of segmentId with itself, because we use it for segment charts in UI
                    and the counts in hierarchy
                 */
                segmentSet.forEach(segment2 -> {
                    List<Object> overLapOfSegmentsInDayKey = ImmutableList.of(day, segment.getId(), segment2.getId());
                    if (segmentCompositionPerDay.containsKey(overLapOfSegmentsInDayKey)) {
                        segmentCompositionPerDay.put(overLapOfSegmentsInDayKey, segmentCompositionPerDay.get(overLapOfSegmentsInDayKey) + 1);
                    } else {
                        segmentCompositionPerDay.put(overLapOfSegmentsInDayKey, 1L);
                    }
                });
            });
        });

    }

    private Map<Instant, Set<Segment>> getTimeToSegmentMap(List<DeviceSegmentHistory> segmentHistories) {

        Map<Instant, Set<Segment>> timeToSegmentMap = new HashMap<>();
        segmentHistories.forEach(deviceSegmentHistory -> {

            Instant day = Instant.ofEpochMilli(deviceSegmentHistory.getSegment().getDateCreatedInMillis())
                    .truncatedTo(ChronoUnit.DAYS);
            timeToSegmentMap.computeIfAbsent(day, e -> new HashSet<>());
            timeToSegmentMap.computeIfPresent(day, (instant, currentDaySegments) -> {
                currentDaySegments.add(deviceSegmentHistory.getSegment());
                return currentDaySegments;
            });

        });
        return timeToSegmentMap;
    }

    public static Map<Instant, Long> getDayCountMap() {
        return dayCountMap;
    }

    public static Map<List<Object>, Long> getSegmentTotalCountMap() {
        return segmentTotalCountMap;
    }

    public static Map<List<Object>, Long> getSegmentCompositionPerDay() {
        return segmentCompositionPerDay;
    }
}
