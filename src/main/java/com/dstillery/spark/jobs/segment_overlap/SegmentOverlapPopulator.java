package com.dstillery.spark.jobs.segment_overlap;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import com.dstillery.common.util.GUtil;
import com.dstillery.spark.common.SparkConfig;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static com.dstillery.spark.common.Util.executeCassandraCommand;
import static com.dstillery.spark.common.Util.getJsonSchemaFromJsonFile;
import static com.dstillery.spark.jobs.CommonUtil.getCurrentSegmentIds;
import static org.apache.spark.sql.functions.date_format;
import static org.apache.spark.sql.functions.from_json;

import jersey.repackaged.com.google.common.collect.ImmutableList;

/*
    This job reads and counts the overlapped counts of all possible segment pair combination and writes them
    to analytics.segment_overlap table in cassandra, the source of data is gicapods.daily_unique_device_bid_eventlog
 */
public class SegmentOverlapPopulator implements Serializable {

    private final static Logger LOGGER = LoggerFactory.getLogger(SegmentOverlapPopulator.class);

    private final SparkConfig sparkConfig;
    private final ReadDeviceSegmentsAndCountTask readDeviceSegmentsAndCountTask;
    private final Instant lookBackInDays;

    public SegmentOverlapPopulator() {
        sparkConfig = new SparkConfig();
        int sleepTime = sparkConfig.getConfigService().getAsInt("job.segmentOverlapPopulator.sleepAfterEachHistoryReadInMillis");

        lookBackInDays = Instant.now().minus(sparkConfig.getConfigService()
                .getAsInt("job.segmentOverlapPopulator.numberOfDaysToLookBackInDays"), ChronoUnit.DAYS);

        readDeviceSegmentsAndCountTask = new ReadDeviceSegmentsAndCountTask(
                sleepTime,
                lookBackInDays,
                sparkConfig.getSparkConf()
        );
    }

    private void countOverlaps() {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "daily_unique_device_bid_eventlog");

        long lookBackInMillis = lookBackInDays.toEpochMilli();
        LOGGER.debug("lookBackInMillis : " + lookBackInMillis);
        //you can infer schema from a sample of event log in a file or create the structType yourself
        //the first way is much easier, the content
        StructType eventSchema = getJsonSchemaFromJsonFile(sparkConfig.getSparkSession(),
                sparkConfig.getConfigService().get("spark.eventLogFilePath") + "/sample-unique-device-eventlog.txt");


        Set<Long> segmentIdCache = getCurrentSegmentIds(sparkConfig.getMySqlDriver());

        Dataset<Row> uniqueDeviceSet = dataSet
                .select(new Column("eventtime"),
                        from_json(new Column("event_in_json"), eventSchema)
                                .getField("eventLog")
                                .getField("device")
                                .getField("deviceId")
                                .as("deviceId"),
                        new Column("device_type")
                )

                .filter((FilterFunction<Row>) row -> row.getTimestamp(0).getTime() >= lookBackInMillis)
                .withColumn("eventtime", date_format(new Column("eventtime"), "yyyy-MM-dd"))
                .withColumnRenamed("eventtime", "hit_date");

        JavaRDD<Row> deviceRdd =
                uniqueDeviceSet
                //note here we group by deviceId so we have one record per deviceId and deviceType
                .groupBy("deviceId", "device_type","hit_date")
                .count()
                .toJavaRDD();

        deviceRdd.coalesce(12);
        LOGGER.debug("deviceRdd size : {}", deviceRdd.count());

        deviceRdd.foreach(readDeviceSegmentsAndCountTask);

        LOGGER.debug("dayCountMap.size : {}", ReadDeviceSegmentsAndCountTask.getDayCountMap().size());
        LOGGER.debug("segmentTotalCountMap.size : {}", ReadDeviceSegmentsAndCountTask.getSegmentTotalCountMap().size());
        LOGGER.debug("segmentCompositionPerDay.size : {}", ReadDeviceSegmentsAndCountTask.getSegmentCompositionPerDay().size());

        ReadDeviceSegmentsAndCountTask.getSegmentCompositionPerDay().forEach((key, overLapCount) -> {

            Instant day = (Instant) key.get(0);
            Long segmentId = (Long) key.get(1);
            Long segment2Id = (Long) key.get(2);
            if (!segmentIdCache.contains(segmentId) || !segmentIdCache.contains(segment2Id)) {
                return;
            }
            Long totalDayCount = ReadDeviceSegmentsAndCountTask.getDayCountMap().get(day);
            Long totalSegment1Count = ReadDeviceSegmentsAndCountTask.getSegmentTotalCountMap().get(ImmutableList.of(day, segmentId));
            Long totalSegment2Count = ReadDeviceSegmentsAndCountTask.getSegmentTotalCountMap().get(ImmutableList.of(day, segment2Id));

            CassandraConnector connector = CassandraConnector.apply(sparkConfig.getSparkConf());
            Session session = connector.openSession();
            // note here that even if we have a record with the same (eventtime, segment1, segment2Id) combination
            // since these are the keys in cassandra table, the record will be overwritten rather than added again.
            String sql = String.format(
                    "insert into analytics.segment_overlap (eventtime, segment1, segment2, total_day_count, over_lap_count, " +
                            " segment1_count, segment2_count ) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                    day.toEpochMilli(),
                    segmentId,
                    segment2Id,
                    totalDayCount, overLapCount, totalSegment1Count, totalSegment2Count);
            ResultSet result = executeCassandraCommand(session, sql);

            if (GUtil.allowedToCall(1000)) {
                LOGGER.debug("sql : {}", sql);
            }
            session.close();
        });

        LOGGER.info("wrote {} records in analytics.segment_overlap", ReadDeviceSegmentsAndCountTask.getSegmentCompositionPerDay().size());
    }




    public static void main(String[] args) {
        SegmentOverlapPopulator updater = new SegmentOverlapPopulator();
        try {
            updater.countOverlaps();
            updater.sparkConfig.getMetricReporterService().addStateModuleForEntity(
                    "JOB_ENDED_WITH_SUCCESS",
                    updater.getClass().getSimpleName(),
                    "ALL");
        } catch (Exception e) {
            LOGGER.error("exception occurred", e);
            updater.sparkConfig.getMetricReporterService().addStateModuleForEntity(
                    "JOB_ENDED_WITH_FAILURE",
                    updater.getClass().getSimpleName(),
                    "ALL");
        }
        updater.sparkConfig.stopSession();
    }

}
