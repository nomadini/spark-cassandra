package com.dstillery.spark.jobs;

import com.dstillery.common.util.GUtil;
import com.dstillery.spark.common.SparkConfig;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static com.dstillery.spark.common.Util.addToFieldsOfSchema;
import static com.dstillery.spark.common.Util.getJsonSchemaFromJsonFile;
import static com.dstillery.spark.common.Util.logRDD;
import static org.apache.spark.sql.functions.date_format;
import static org.apache.spark.sql.functions.explode;
import static org.apache.spark.sql.functions.from_json;

/*
    This job reads and counts the number of unique device counts and normal device counts
     and populates them in mysql
    mango reads the segment stats to show the segment counts
 */
public class SegmentCountPopulator {

    private final static Logger LOGGER = LoggerFactory.getLogger(SegmentCountPopulator.class);
    private final SparkConfig sparkConfig;

    public SegmentCountPopulator() {
        sparkConfig = new SparkConfig();
    }

    private void findSegmentCountsInLast24Hours() {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "daily_unique_device_bid_eventlog");
        Instant time = Instant.now().minus(
                sparkConfig.getConfigService().getAsInt("job.segmentCountPopulator.numberOfDaysToBackFill"), ChronoUnit.DAYS);
        long lookBackTimeInMillis = time.toEpochMilli();
        LOGGER.debug("lookBackTimeInMillis : " + lookBackTimeInMillis);
        //you can infer schema from a sample of event log in a file or create the structType yourself
        //the first way is much easier, the content
        //sample-event-log.txt exists in src/main/resources directory, in case it's deleted from /tmp dir
        StructType eventSchema = getJsonSchemaFromJsonFile(sparkConfig.getSparkSession(),
                sparkConfig.getConfigService().get("spark.eventLogFilePath") + "/sample-unique-device-eventlog.txt");
        LOGGER.debug("eventDataSetSample printSchema");

        long numberOfUniqueDevicesSeen = dataSet
                .select(new Column("eventtime"))
                .filter((FilterFunction<Row>) row -> {
                    long rowTime = row.getTimestamp(0).getTime();

                    boolean qualify = row.getTimestamp(0).getTime() >= lookBackTimeInMillis;

                    if (GUtil.allowedToCall(1000)) {
                        LOGGER.debug("lookBackTimeInMillis : {}, rowTime : {}, qualify : {}",
                                lookBackTimeInMillis, rowTime, qualify);
                    }

                    return qualify;
                }).count();

        LOGGER.info("numberOfUniqueDevicesSeen : {}", numberOfUniqueDevicesSeen);

        Dataset<Row> segmentDataSet = dataSet
                .select(new Column("eventtime"),
                        from_json(new Column("event_in_json"), eventSchema)
                                .getField("eventLog")
                                .getField("allSegmentIdsFoundList")
                                .as("allSegmentIdsFoundList")
                        /*
                         * this is how we get deviceId from eventLog as a column

                        from_json(new Column("event_in_json"), eventSchema)
                                .getField("eventLog")
                                .getField("device")
                                .getField("deviceId")
                                .as("deviceId")
                         */

                )
                .filter((FilterFunction<Row>) row -> {
                    long rowTime = row.getTimestamp(0).getTime();

                    boolean qualify = row.getTimestamp(0).getTime() >= lookBackTimeInMillis;
                    if (GUtil.allowedToCall(1000)) {
                        LOGGER.debug("lookBackTimeInMillis : {}, rowTime : {}, qualify : {}",
                                lookBackTimeInMillis, rowTime, qualify);
                    }

                    return qualify;
                })
                .withColumn("eventtime", date_format(new Column("eventtime"), "yyyy-MM-dd"))
                .withColumnRenamed("eventtime", "hit_date")
                .withColumn("allSegmentIdsFoundList", explode(new Column("allSegmentIdsFoundList")));


        segmentDataSet.limit(10).show();

        JavaRDD<Row> tgStatsRDD =
                segmentDataSet
                .groupBy("hit_date", "allSegmentIdsFoundList")
                .count()
                .withColumn("qualification_probability", new Column("count").divide(numberOfUniqueDevicesSeen))
                .toJavaRDD();

        List<StructField> fields = new ArrayList<>();
        addToFieldsOfSchema(fields, "hit_date", DataTypes.StringType);
        addToFieldsOfSchema(fields, "segment_id", DataTypes.LongType);
        addToFieldsOfSchema(fields, "normal_counts", DataTypes.LongType);
        addToFieldsOfSchema(fields, "qualification_probability", DataTypes.DoubleType);

        StructType schema = DataTypes.createStructType(fields);
        LOGGER.info("numberOfPartitions : {}", tgStatsRDD.getNumPartitions());

        LOGGER.info("tgStatsRDD content");
        logRDD(tgStatsRDD);

        Dataset<Row> impRecords = sparkConfig.getSqlContext()
                .createDataFrame(tgStatsRDD, schema);

        List<Row> rows = impRecords.collectAsList();
        LOGGER.debug("created number Of records : {}", rows.size());

        String tableName = "segment_counts";
        sparkConfig.getMySqlDriver().runSqlCommand(
                "delete from segment_counts where UNIX_TIMESTAMP(created_at) >= " + lookBackTimeInMillis / 1000);
        /*
         * for some unknown reason, this job writes the data in mysql twice. (the reduce part of the job gets executed twice)
         * i have added a unique check on segment_counts table to avoid the extra rows.
         * if you see the exception that says duplicate key in mysql, pelase ignore it.
         */
        sparkConfig.getMySqlDriver().appendDataSetInMysql(tableName, impRecords);
    }

    public static void main(String[] args) {
        SegmentCountPopulator updater = new SegmentCountPopulator();
        try {
            updater.findSegmentCountsInLast24Hours();
            updater.sparkConfig.getMetricReporterService().addStateModuleForEntity(
                    "JOB_ENDED_WITH_SUCCESS",
                    updater.getClass().getSimpleName(),
                    "ALL");
        } catch (Exception e) {
            LOGGER.error("exception occurred", e);
            updater.sparkConfig.getMetricReporterService().addStateModuleForEntity(
                    "JOB_ENDED_WITH_FAILURE",
                    updater.getClass().getSimpleName(),
                    "ALL");
        }
        updater.sparkConfig.stopSession();
    }
}
