package com.dstillery.spark.jobs;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.spark.common.SparkConfig;
import com.dstillery.spark.jobs.geoanalysis.MGRSDetail;
import com.dstillery.spark.jobs.geoanalysis.geojson.MGRStoGeoJsonConverter;
import com.dstillery.spark.jobs.geoanalysis.geojson.MGRStoGeoJsonConverter.GeoFeatureType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import scala.Tuple2;

public class TopMGRS100FinderPerSegment {

    private final static Logger LOGGER = LoggerFactory.getLogger(TopMGRS100FinderPerSegment.class);
    private final SparkConfig sparkConfig;
    private final JedisPool jedisPool;
    private MGRStoGeoJsonConverter converter = new MGRStoGeoJsonConverter();

    public TopMGRS100FinderPerSegment() {
        sparkConfig = new SparkConfig();
        jedisPool = new JedisPool(new JedisPoolConfig(), "localhost");
    }

    public static void main(String[] args) throws Exception {

        TopMGRS100FinderPerSegment topMGRS100FinderPerSegment = new TopMGRS100FinderPerSegment();
        //for now we set it to 2400 hours, because we don't have enought data per day
        //TODO change it to 24 or 3h  when in real production
        topMGRS100FinderPerSegment.saveSegmentIdsToTopMGRSInLastNHours(72);
    }

    private void saveSegmentIdsToTopMGRSInLastNHours(int hoursAgo) throws JsonProcessingException {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "bideventlog");
        long lastNHours = Instant.now().minus(hoursAgo, ChronoUnit.HOURS).toEpochMilli();
        LOGGER.info("finding top data for : lastNHours : {}", lastNHours);

        JavaRDD<Row> dataMainRdd = dataSet.select(
                "eventtime",
                "allSegmentIdsFoundList",
                "mgrs100m"
        )
                .filter((FilterFunction<Row>) row -> row.getTimestamp(0).getTime() >= lastNHours)
                .groupBy("allSegmentIdsFoundList", "mgrs100m")

                .count()
                .toJavaRDD();
        //we are grouping all the list of segmentIds , MGRS100 entries
        //by a single segmentId Key here...
        List<Tuple2<Long, Iterable<TGSegmentResult>>> tgSegmentResultsGroupBySegmentIds =
                dataMainRdd.flatMap(new FlatMapper().invoke())
                .groupBy(result -> {
                    return result.getSegmentId();
                })
                .collect();

        Set<Long> allSegmentIdsRecorded = tgSegmentResultsGroupBySegmentIds.stream().map(entity -> entity._1)
                .collect(Collectors.toSet());
        LOGGER.info("allSegmentIdsRecorded : {}", allSegmentIdsRecorded);


        List<SegmentIdToMgrsGroup> topMGRSForSegments = tgSegmentResultsGroupBySegmentIds.stream().map(group -> {
            List<String> allMGrs  = new ArrayList<>();
            while(group._2.iterator().hasNext()) {
                TGSegmentResult currentResult = group._2.iterator().next();
                allMGrs.add(currentResult.getMgrs100m());
            }
            return new SegmentIdToMgrsGroup(group._1, allMGrs);
        })
                .filter(group -> group.allMGrs.size() >= 1)//TODO increase this limit later, or make it based on histogram of sizes
                .collect(Collectors.toList());



        for (SegmentIdToMgrsGroup group : topMGRSForSegments) {
            createGeoJsonForSegmentIdAndCacheIt(group);
        }

    }

    private void createGeoJsonForSegmentIdAndCacheIt(SegmentIdToMgrsGroup topMGRSForSegments) throws JsonProcessingException {
        List<MGRSDetail> listOfMgrs = topMGRSForSegments.allMGrs.stream()
                .map(mgrs100 -> {
                    MGRSDetail detail = new MGRSDetail(
                            mgrs100,
                            String.valueOf(topMGRSForSegments.segmentId));
                    return detail;
                }).collect(Collectors.toList());

        //these MGRS values are MGRS100 to we choose a 0.062138 mile for the radius of polygon to give us a 100m to 100m polygon
        String featureCollectionGeoJson = converter.convert(listOfMgrs, 0.062138, GeoFeatureType.POINT);

        String segmentGeoFeatureCollectionKeyInRedis = "topMGRSForSegments_last_24hr_"+topMGRSForSegments.segmentId;
        // we save this big json in redis on r3
        try (Jedis jedis = jedisPool.getResource()) {
            LOGGER.info("setting cache entry with key : {}", segmentGeoFeatureCollectionKeyInRedis);
            jedis.set(segmentGeoFeatureCollectionKeyInRedis, featureCollectionGeoJson);
            //1 day expiry
            jedis.expire(segmentGeoFeatureCollectionKeyInRedis, 3600 * 24);
        }
    }
}

class FlatMapper implements Serializable {
    private final static Logger LOGGER = LoggerFactory.getLogger(FlatMapper.class);
    private ObjectMapper objectMapper = new ObjectMapper();

    public FlatMapFunction<Row, TGSegmentResult> invoke() {

        return new FlatMapFunction<Row, TGSegmentResult>() {
            @Override
            public Iterator<TGSegmentResult> call(Row row) throws Exception {
                //each row has a list of segmentIds  and an MGRS100 value,
                //we flat map it in this function, and return a list of segmentId to MGRS100
                List<TGSegmentResult> array = new ArrayList<>();
                String segmentValue = row.getString(0);
                if (segmentValue == null) {
                    return array.iterator();
                }
                LOGGER.info("segmentValue : {}", segmentValue);
                List<Long> segmentIds = objectMapper.readValue(segmentValue, List.class);
                segmentIds.forEach(segmentId -> {
                    TGSegmentResult result = new TGSegmentResult(
                            row.getTimestamp(0),
                            segmentId,
                            row.getString(2)
                    );
                    array.add(result);

                });
                return array.iterator();
            }
        };
    }
}

class SegmentIdToMgrsGroup implements Serializable {
    List<String> allMGrs;
    Long segmentId;
    public SegmentIdToMgrsGroup(Long segmentId, List<String> allMGrs) {
        this.segmentId = segmentId;
        this.allMGrs = allMGrs;
    }
}

class TGSegmentResult implements Serializable {
    private Timestamp eventtime;
    private Long segmentId;
    private String mgrs100m;

    public TGSegmentResult(Timestamp timestamp, Long segmentId, String mgrs100m) {
        this.eventtime = timestamp;
        this.segmentId = segmentId;
        this.mgrs100m = mgrs100m;
    }

    public Timestamp getEventtime() {
        return eventtime;
    }

    public Long getSegmentId() {
        return segmentId;
    }

    public String getMgrs100m() {
        return mgrs100m;
    }
}

class TopMGRS100FinderPerSegmentCountColumnComparator implements Comparator<Row>, Serializable {
    @Override
    public int compare(Row o1, Row o2) {
        if (o1.getLong(2) == o2.getLong(2)) {
            return 0;
        }
        if (o1.getLong(2) > o2.getLong(2)) {
            return -1;
        }
        return 1;
    }
}
