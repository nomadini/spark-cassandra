package com.dstillery.spark.jobs;

import static com.dstillery.spark.common.Util.saveRDDInFile;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.DoubleFunction;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.spark.common.SparkConfig;

import scala.Tuple2;

public class TopDeviceAndIpAndMrgs10SeenProducer {

    private final static Logger LOGGER = LoggerFactory.getLogger(TopDeviceAndIpAndMrgs10SeenProducer.class);
    //good documentation on how to read data from cassandra :
    //https://github.com/datastax/spark-cassandra-connector/blob/master/doc/14_data_frames.md
    //https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/sql/JavaSparkSQLExample.java
    private final SparkConfig sparkConfig;

    public TopDeviceAndIpAndMrgs10SeenProducer() {
        sparkConfig = new SparkConfig();
    }

    private void saveTopNDevicesInLastXHours(int hoursAgo, String folderName, String keyOfData) {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "bideventlog");
        long lastNHours = Instant.now().minus(hoursAgo, ChronoUnit.HOURS).toEpochMilli();
        LOGGER.info("finding top data for : keyOfData : {}, folderName : {}, lastNHours : {}",keyOfData, folderName, lastNHours);

        Dataset<Row> sumOfCounts = dataSet
                .select("eventtime",
                        keyOfData,
                        "count"
                )
                .filter((FilterFunction<Row>) row -> row.getTimestamp(0).getTime() >= lastNHours)
                .groupBy(keyOfData)
                .sum("count");


        JavaRDD<Row> deviceCountsRDD = sumOfCounts
        //first we need to sum the counts and then filter
                .filter((FilterFunction<Row>) row -> row.getLong(1) > 1)
                .sort("sum(count)")
                .toJavaRDD();
        if (deviceCountsRDD.count() <= 0) {
            LOGGER.info("aborting. haven't found busy data for : {} key ", keyOfData);
            return;
        }

        JavaDoubleRDD countsAsDoubles = deviceCountsRDD.mapToDouble(
                (DoubleFunction<Row>) x -> x.getLong(1));

        //this would be 12.5 % per bucket
        double bucketValue = getTopXBucketValue(countsAsDoubles, 8);

        LOGGER.info("value of top bucket 12.5% is : {}", bucketValue);
        JavaRDD<Row> topDevicesMostSeen = sumOfCounts.filter((FilterFunction<Row>)
                row -> row.getLong(1) >= bucketValue)
                .sort("sum(count)")
                .toJavaRDD();

        saveRDDInFile(topDevicesMostSeen, folderName);
    }

    private double getTopXBucketValue(JavaDoubleRDD countsRDD, int bucketCount) {
        List<Double> allBucketValues = new ArrayList<>();
        double maxBucketValue = 0;
        Tuple2<double[], long[]> histogram = countsRDD.histogram(bucketCount);

        for (int i = 0; i <= histogram._1.length - 1; i++) {
            double bucketValue = histogram._1[i];
            allBucketValues.add(bucketValue);
            if (bucketValue >= maxBucketValue) {
                maxBucketValue = bucketValue;
            }
            if (histogram._1.length == i + 1) {
                LOGGER.info("all bucket values are : {}, returning top bucket value : {}, maxBucketValue : {}",
                        allBucketValues, bucketValue, maxBucketValue);
                if (bucketValue != maxBucketValue) {
                    throw new IllegalStateException("max bucket value is not selected");
                }
                return bucketValue;
            }
        }
        throw new IllegalStateException("cannot find top bucket value");
    }

    public static void main(String[] args) {
        TopDeviceAndIpAndMrgs10SeenProducer topDeviceAndIpAndMrgs10SeenProducer = new TopDeviceAndIpAndMrgs10SeenProducer();
        topDeviceAndIpAndMrgs10SeenProducer.saveTopNDevicesInLastXHours(168, "/tmp/top_common_devices_last7days", "nomadinideviceid");
        topDeviceAndIpAndMrgs10SeenProducer.saveTopNDevicesInLastXHours(168, "/tmp/top_common_ips_last7days", "deviceIp");
        topDeviceAndIpAndMrgs10SeenProducer.saveTopNDevicesInLastXHours(168, "/tmp/top_common_mgrs10m_last7days", "mgrs10m");
        topDeviceAndIpAndMrgs10SeenProducer.sparkConfig.stopSession();
    }

}


