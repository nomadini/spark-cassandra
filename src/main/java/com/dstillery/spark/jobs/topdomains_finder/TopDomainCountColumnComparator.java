package com.dstillery.spark.jobs.topdomains_finder;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.spark.sql.Row;

public class TopDomainCountColumnComparator implements Comparator<Row>, Serializable {
    @Override
    public int compare(Row o1, Row o2) {
        if (o1.getLong(2) == o2.getLong(2)) {
            return 0;
        } else {
            return o1.getLong(2) > o2.getLong(2) ? 1 : -1;
        }
    }
}
