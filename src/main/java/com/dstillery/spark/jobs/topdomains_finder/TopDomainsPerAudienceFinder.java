package com.dstillery.spark.jobs.topdomains_finder;

import static com.dstillery.spark.common.Util.executeCassandraCommand;
import static com.dstillery.spark.common.Util.getJsonSchemaFromJsonFile;
import static com.dstillery.spark.jobs.CommonUtil.getCurrentSegmentIds;
import static org.apache.spark.sql.functions.date_format;
import static org.apache.spark.sql.functions.explode;
import static org.apache.spark.sql.functions.from_json;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import com.dstillery.common.segment.segment_top_entity.DomainCount;
import com.dstillery.common.util.GUtil;
import com.dstillery.spark.common.SparkConfig;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
    This job reads the event logs and finds the top domains per segments
 */
public class TopDomainsPerAudienceFinder {

    private final static Logger LOGGER = LoggerFactory.getLogger(TopDomainsPerAudienceFinder.class);
    //good documentation on how to read data from cassandra :
    //https://github.com/datastax/spark-cassandra-connector/blob/master/doc/14_data_frames.md
    //https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/sql/JavaSparkSQLExample.java
    private final SparkConfig sparkConfig;
    private int minimumCountOfDomainsToChoose = 10;
    public TopDomainsPerAudienceFinder(Map<String, String> arguments) {
        sparkConfig = new SparkConfig(arguments);
    }

    private void findTopDomains() {
        Set<Long> segmentIdCache = getCurrentSegmentIds(sparkConfig.getMySqlDriver());

        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "daily_unique_device_bid_eventlog");
        long lookBackInMillis = Instant.now().minus(
                sparkConfig.getConfigService().getAsInt("job.topDomainsPerAudienceFinder.numberOfDaysToBackFill"),
                ChronoUnit.DAYS).toEpochMilli();

        minimumCountOfDomainsToChoose = sparkConfig.getConfigService().getAsInt("job.topDomainsPerAudienceFinder.minimumCountOfDomainsToChoose");
        LOGGER.debug("lookBackInMillis : " + lookBackInMillis);
        //you can infer schema from a sample of event log in a file or create the structType yourself
        //the first way is much easier, the content
        //sample-event-log.txt exists in src/main/resources directory, in case it's deleted from /tmp dir
        StructType eventSchema = getJsonSchemaFromJsonFile(sparkConfig.getSparkSession(),
                sparkConfig.getConfigService().get("spark.eventLogFilePath") + "/sample-unique-device-eventlog.txt");

        List<Row> topDomains = dataSet
                .select(new Column("eventtime"),
                        from_json(new Column("event_in_json"), eventSchema)
                                .getField("eventLog")
                                .getField("allSegmentIdsFoundList")
                                .as("allSegmentIdsFoundList"),
                        from_json(new Column("event_in_json"), eventSchema)
                                .getField("eventLog")
                                .getField("siteDomain")
                                .as("domains")
                )
                .filter((FilterFunction<Row>) row -> {
                    long rowTime = row.getTimestamp(0).getTime();

                    boolean qualify = row.getTimestamp(0).getTime() >= lookBackInMillis;
                    if (GUtil.allowedToCall(1000)) {
                        LOGGER.debug("lookBackInMillis : {}, rowTime : {}, qualify : {}, row : {}",
                                lookBackInMillis, rowTime, qualify, row);
                    }

                    //doesnt have number in it, is pure ascii code and has more than 4 characters
                    return qualify;
                })
                .withColumn("eventtime", date_format(new Column("eventtime"), "yyyy-MM-dd"))
                .withColumnRenamed("eventtime", "hit_date")
                .withColumn("allSegmentIdsFoundList", explode(new Column("allSegmentIdsFoundList")))
                .withColumn("domains", new Column("domains"))
                .filter(row -> {
                    String domain = row.getString(2);
                    Long segment = row.getLong(1);
                    return segment != 0 &&
                            domain != null &&
                            !domain.isEmpty();
                })

                .groupBy("allSegmentIdsFoundList", "domains")
                .count()
                .toJavaRDD().collect();

        Map<Long, List<Long>> segmentToTopNCounts = getSegmentToTopNCountsMap(topDomains, segmentIdCache);

        //now we remove all the domains with less count than top 5 counts
        topDomains = pickDomainsWithEnoughCounts(topDomains, segmentToTopNCounts);
        LOGGER.debug("topDomains : {}", topDomains);

        Instant day = Instant.now().truncatedTo(ChronoUnit.DAYS);

        ObjectMapper objectMapper = new ObjectMapper();
        Map<Long, List<DomainCount>> topDomainsList = getSegmentToTopDomainMap(topDomains);
        topDomainsList.forEach((segmentId, domains) -> {
            if (!segmentIdCache.contains(segmentId)) {
                return;
            }
            trimLongDomainsList(domains);

            CassandraConnector connector = CassandraConnector.apply(sparkConfig.getSparkConf());
            Session session = connector.openSession();
            String sql = null;
            try {
                sql = String.format(
                        "insert into analytics.segment_top_entities (eventtime, segment, top_features) VALUES (%s, %s, '%s')",
                        day.toEpochMilli(),
                        segmentId,
                        objectMapper.writeValueAsString(domains));
            } catch (JsonProcessingException e) {
                //ignore
            }
            ResultSet result = executeCassandraCommand(session, sql);

            LOGGER.debug("sql : {}", sql);
            session.close();
        });

        LOGGER.info("wrote {} records in analytics.segment_top_entities", topDomainsList.size());
    }

    private void trimLongDomainsList(List<DomainCount> domains) {
        //pick the top ten domainCount objects
        if (domains.size() > 10) {
            domains.sort((o1, o2) -> {

                if (o1.getCount().equals(o2.getCount())) {
                    return 0;
                }
                return o1.getCount() > o2.getCount() ? -1 : 1; //DESCENDING
            });

            List<DomainCount> originalDomains = new ArrayList<>(domains);
            while (domains.size() >= 10) {
                domains.remove(9);
            }

            LOGGER.debug("trimming domains to top 10 values, from {} to {}", originalDomains, domains);
        }
    }

    private List<Row> pickDomainsWithEnoughCounts(List<Row> rowsOfSegmentDomainCount, Map<Long, List<Long>> segmentToTopNCounts) {
        rowsOfSegmentDomainCount = rowsOfSegmentDomainCount.stream().filter(row -> {

                    Long segment = row.getLong(0);
                    Long count = row.getLong(2);
                    if (count < minimumCountOfDomainsToChoose) {
                        //we don't to pick domains with count less than minimumCountOfDomainsToChoose
                        //too many unuseful data
                        return false;
                    }
                    List<Long> topNCounts = segmentToTopNCounts.get(segment);
                return topNCounts!= null && topNCounts.get(topNCounts.size() - 1) <= count;
        }).collect(Collectors.toList());
        return rowsOfSegmentDomainCount;
    }

    private Map<Long, List<Long>> getSegmentToTopNCountsMap(List<Row> topDomains, Set<Long> segmentIdCache) {
        Map<Long, List<Long>> segmentToTopNCounts = new HashMap<>();
        topDomains.forEach(row -> {
            Long segment = row.getLong(0);
            String domain = row.getString(1);
            Long count = row.getLong(2);
            if (!segmentIdCache.contains(segment)) {
                return;
            }
            segmentToTopNCounts.computeIfAbsent(segment, s -> new ArrayList<>());
            segmentToTopNCounts.computeIfPresent(segment, new BiFunction<Long, List<Long>, List<Long>>() {
                @Override
                public List<Long> apply(Long s, List<Long> currentCounts) {
                    //we add a count to the list of counts for each segment and sort it descending
                    currentCounts.add(count);
                    currentCounts.sort((o1, o2) -> {
                        if (o1.equals(o2)) {
                            return 0;
                        }
                        if (o1 > o2) {
                            return -1;
                        }
                        return 1;
                    });
                    //we keep the top 5 counts and remove the rest
                    while (currentCounts.size() > 5) {
                        currentCounts.remove(currentCounts.size() - 1);
                    }

                    LOGGER.debug("segment : {}, currentCounts: {}", segment, currentCounts);
                    return currentCounts;
                }
            });

        });
        return segmentToTopNCounts;
    }

    private Map<Long, List<DomainCount>> getSegmentToTopDomainMap(List<Row> topDomains) {
        Map<Long, List<DomainCount>> segmentToTopDomains = new HashMap<>();
        topDomains.forEach(row -> {
            Long segment = row.getLong(0);
            String domain = row.getString(1);
            Long count = row.getLong(2);

            segmentToTopDomains.computeIfAbsent(segment, s -> new ArrayList<>());
            segmentToTopDomains.computeIfPresent(segment, new BiFunction<Long, List<DomainCount>, List<DomainCount>>() {
                @Override
                public List<DomainCount> apply(Long s, List<DomainCount> currentCounts) {
                    //we add a count to the list of counts for each segment and sort it descending
                    currentCounts.add(new DomainCount(domain, count));
                    LOGGER.debug("segment : {}, currentCounts: {}", segment, currentCounts);
                    return currentCounts;
                }
            });

        });
        LOGGER.debug("segmentToTopDomains : {}", segmentToTopDomains);
        return segmentToTopDomains;
    }

    public static void main(String[] args) {
        LOGGER.info("program arguments, args : {}", Arrays.asList(args));
        Map<String, String> arguments = new HashMap<>();
        Arrays.stream(args).forEach(arg -> {
            String[] keyValue = arg.split("=");
            if (keyValue.length == 2) {
                arguments.put(keyValue[0], keyValue[1]);
            }
        });

        TopDomainsPerAudienceFinder updater = new TopDomainsPerAudienceFinder(arguments);
        try {
            updater.findTopDomains();
            updater.sparkConfig.getMetricReporterService().addStateModuleForEntity(
                "JOB_ENDED_WITH_SUCCESS",
                updater.getClass().getSimpleName(),
                "ALL");
        } catch (Exception e) {
            LOGGER.error("exception occurred", e);
            updater.sparkConfig.getMetricReporterService().addStateModuleForEntity(
                    "JOB_ENDED_WITH_FAILURE",
                    updater.getClass().getSimpleName(),
                    "ALL");
        }
        updater.sparkConfig.stopSession();
    }
}
