package com.dstillery.spark.jobs.topkeywords_finder;

import static com.dstillery.spark.common.Util.executeCassandraCommand;
import static com.dstillery.spark.common.Util.getJsonSchemaFromJsonFile;
import static com.dstillery.spark.jobs.CommonUtil.getCurrentSegmentIds;
import static org.apache.spark.sql.functions.date_format;
import static org.apache.spark.sql.functions.explode;
import static org.apache.spark.sql.functions.from_json;

import java.nio.charset.Charset;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import com.dstillery.common.segment.segment_top_entity.KeyWordCount;
import com.dstillery.common.util.GUtil;
import com.dstillery.spark.common.SparkConfig;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
    This job reads the event logs and finds the top keywords per segments
 */
public class TopKeywordsPerAudienceFinder {

    private final static Logger LOGGER = LoggerFactory.getLogger(TopKeywordsPerAudienceFinder.class);
    //good documentation on how to read data from cassandra :
    //https://github.com/datastax/spark-cassandra-connector/blob/master/doc/14_data_frames.md
    //https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/sql/JavaSparkSQLExample.java
    private final SparkConfig sparkConfig;
    private int minimumCountOfKeyWordsToChoose = 10;
    public TopKeywordsPerAudienceFinder(Map<String, String> arguments) {
        sparkConfig = new SparkConfig(arguments);
    }

    private void findTopKeyWords() {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "daily_unique_device_bid_eventlog");
        long lookBackInMillis = Instant.now().minus(
                sparkConfig.getConfigService().getAsInt("job.TopKeywordsPerAudienceFinder.numberOfDaysToBackFill"),
                ChronoUnit.DAYS).toEpochMilli();
        Set<Long> segmentIdCache = getCurrentSegmentIds(sparkConfig.getMySqlDriver());

        minimumCountOfKeyWordsToChoose = sparkConfig.getConfigService().getAsInt("job.topKeywordsPerAudienceFinder.minimumCountOfKeyWordsToChoose");
        LOGGER.debug("lookBackInMillis : " + lookBackInMillis);
        //you can infer schema from a sample of event log in a file or create the structType yourself
        //the first way is much easier, the content
        //sample-event-log.txt exists in src/main/resources directory, in case it's deleted from /tmp dir
        StructType eventSchema = getJsonSchemaFromJsonFile(sparkConfig.getSparkSession(),
                sparkConfig.getConfigService().get("spark.eventLogFilePath") + "/sample-unique-device-eventlog.txt");
        LOGGER.debug("eventDataSetSample printSchema");

        List<Row> topKeywords = dataSet
                .select(new Column("eventtime"),
                        from_json(new Column("event_in_json"), eventSchema)
                                .getField("eventLog")
                                .getField("allSegmentIdsFoundList")
                                .as("allSegmentIdsFoundList"),
                        from_json(new Column("event_in_json"), eventSchema)
                                .getField("eventLog")
                                .getField("keyWords")
                                .as("keyWords")
                )
                .filter((FilterFunction<Row>) row -> {
                    long rowTime = row.getTimestamp(0).getTime();

                    boolean qualify = row.getTimestamp(0).getTime() >= lookBackInMillis;
                    if (GUtil.allowedToCall(1000)) {
                        LOGGER.debug("lookBackInMillis : {}, rowTime : {}, qualify : {}, row : {}",
                                lookBackInMillis, rowTime, qualify, row);
                    }

                    //doesnt have number in it, is pure ascii code and has more than 4 characters
                    return qualify;
                })
                .withColumn("eventtime", date_format(new Column("eventtime"), "yyyy-MM-dd"))
                .withColumnRenamed("eventtime", "hit_date")
                .withColumn("allSegmentIdsFoundList", explode(new Column("allSegmentIdsFoundList")))
                .withColumn("keyWords", explode(new Column("keyWords")))
                .filter(row -> {
                    String keyWord = row.getString(2);
                    Long segment = row.getLong(1);
                    return segment != 0 &&
                            keyWord != null &&
                            !keyWord.isEmpty() &&
                            !keyWord.matches(".*\\d.*") &&
                            Charset.forName("US-ASCII").newEncoder().canEncode(keyWord) &&
                            keyWord.length() >= 4;
                })

                .groupBy("allSegmentIdsFoundList", "keyWords")
                .count()
                .toJavaRDD().collect();

        Map<Long, List<Long>> segmentToTopNCounts = getSegmentToTopNCountsMap(topKeywords, segmentIdCache);

        //now we remove all the keywords with less count than top 5 counts
        topKeywords = pickKeywordsWithEnoughCounts(topKeywords, segmentToTopNCounts);
        LOGGER.debug("topKeywords : {}", topKeywords);

        Instant day = Instant.now().truncatedTo(ChronoUnit.DAYS);

        ObjectMapper objectMapper = new ObjectMapper();
        Map<Long, List<KeyWordCount>> topKeywordsList = getSegmentToTopKeyWordMap(topKeywords);
        topKeywordsList.forEach((segmentId, keywords) -> {
            if (!segmentIdCache.contains(segmentId)) {
                return;
            }

            trimLongKeywordsList(keywords);

            CassandraConnector connector = CassandraConnector.apply(sparkConfig.getSparkConf());
            Session session = connector.openSession();
            String sql = null;
            try {
                sql = String.format(
                        "insert into analytics.segment_top_entities (eventtime, segment, top_key_words) VALUES (%s, %s, '%s')",
                        day.toEpochMilli(),
                        segmentId,
                        objectMapper.writeValueAsString(keywords));
            } catch (JsonProcessingException e) {
                //ignore
            }
            ResultSet result = executeCassandraCommand(session, sql);

            LOGGER.debug("sql : {}", sql);
            session.close();
        });

        LOGGER.info("wrote {} records in analytics.segment_top_entities", topKeywordsList.size());
    }

    private void trimLongKeywordsList(List<KeyWordCount> keywords) {
        //pick the top ten keyWordCount objects
        if (keywords.size() > 10) {
            keywords.sort((o1, o2) -> {

                if (o1.getCount().equals(o2.getCount())) {
                    return 0;
                }
                return o1.getCount() > o2.getCount() ? -1 : 1; //DESCENDING
            });

            List<KeyWordCount> originalKeyWords = new ArrayList<>(keywords);
            while (keywords.size() >= 10) {
                keywords.remove(9);
            }

            LOGGER.debug("trimming keywords to top 10 values, from {} to {}", originalKeyWords, keywords);
        }
    }

    private List<Row> pickKeywordsWithEnoughCounts(List<Row> rowsOfSegmentKeyWordCount,
                                                   Map<Long, List<Long>> segmentToTopNCounts) {
        rowsOfSegmentKeyWordCount = rowsOfSegmentKeyWordCount.stream().filter(row -> {

                    Long segmentId = row.getLong(0);
                    Long count = row.getLong(2);
                    if (count < minimumCountOfKeyWordsToChoose) {
                        //we don't to pick keywords with count less than minimumCountOfKeyWordsToChoose
                        //too many unuseful data
                        return false;
                    }
                    List<Long> topNCounts = segmentToTopNCounts.get(segmentId);
                    return topNCounts != null && topNCounts.get(topNCounts.size() - 1) <= count;
        }).collect(Collectors.toList());
        return rowsOfSegmentKeyWordCount;
    }

    private Map<Long, List<Long>> getSegmentToTopNCountsMap(List<Row> topKeywords, Set<Long> segmentIdCache) {
        Map<Long, List<Long>> segmentToTopNCounts = new HashMap<>();
        topKeywords.forEach(row -> {
            Long segmentId = row.getLong(0);
            String keyWord = row.getString(1);
            Long count = row.getLong(2);

            if (!segmentIdCache.contains(segmentId)) {
                return;
            }
            segmentToTopNCounts.computeIfAbsent(segmentId, s -> new ArrayList<>());
            segmentToTopNCounts.computeIfPresent(segmentId, (s, currentCounts) -> {
                //we add a count to the list of counts for each segment and sort it descending
                currentCounts.add(count);
                currentCounts.sort((o1, o2) -> {
                    if (o1.equals(o2)) {
                        return 0;
                    }
                    if (o1 > o2) {
                        return -1;
                    }
                    return 1;
                });
                //we keep the top 5 counts and remove the rest
                while (currentCounts.size() > 5) {
                    currentCounts.remove(currentCounts.size() - 1);
                }

                LOGGER.debug("segment : {}, topFiveCounts: {}", segmentId, currentCounts);
                return currentCounts;
            });

        });
        return segmentToTopNCounts;
    }

    private Map<Long, List<KeyWordCount>> getSegmentToTopKeyWordMap(List<Row> topKeywords) {
        Map<Long, List<KeyWordCount>> segmentToTopKeyWords = new HashMap<>();
        topKeywords.forEach(row -> {
            Long segment = row.getLong(0);
            String keyWord = row.getString(1);
            Long count = row.getLong(2);

            segmentToTopKeyWords.computeIfAbsent(segment, s -> new ArrayList<>());
            segmentToTopKeyWords.computeIfPresent(segment, new BiFunction<Long, List<KeyWordCount>, List<KeyWordCount>>() {
                @Override
                public List<KeyWordCount> apply(Long s, List<KeyWordCount> currentCounts) {
                    //we add a count to the list of counts for each segment and sort it descending
                    currentCounts.add(new KeyWordCount(keyWord, count));
                    LOGGER.debug("segment : {}, currentCounts: {}", segment, currentCounts);
                    return currentCounts;
                }
            });

        });
        segmentToTopKeyWords.forEach((segment, keyWordCounts) -> {
            LOGGER.debug("segment : {}, keyWordCounts : {}", keyWordCounts);
        });
        LOGGER.debug("segmentToTopKeyWords.size() : {}", segmentToTopKeyWords.size());
        return segmentToTopKeyWords;
    }

    public static void main(String[] args) {
        LOGGER.info("program arguments, args : {}", Arrays.asList(args));
        Map<String, String> arguments = new HashMap<>();
        Arrays.stream(args).forEach(arg -> {
            String[] keyValue = arg.split("=");
            if (keyValue.length == 2) {
                arguments.put(keyValue[0], keyValue[1]);
            }
        });

        TopKeywordsPerAudienceFinder updater = new TopKeywordsPerAudienceFinder(arguments);
        try {
            updater.findTopKeyWords();
            updater.sparkConfig.getMetricReporterService().addStateModuleForEntity(
                    "JOB_ENDED_WITH_SUCCESS",
                    updater.getClass().getSimpleName(),
                    "ALL");
        } catch (Exception e) {
            LOGGER.error("exception occurred", e);
            updater.sparkConfig.getMetricReporterService().addStateModuleForEntity(
                    "JOB_ENDED_WITH_FAILURE",
                    updater.getClass().getSimpleName(),
                    "ALL");
        }
        updater.sparkConfig.stopSession();
    }
}
