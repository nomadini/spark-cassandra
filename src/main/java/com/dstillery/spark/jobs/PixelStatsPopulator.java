package com.dstillery.spark.jobs;

import static com.dstillery.spark.common.Util.addToFieldsOfSchema;
import static com.dstillery.spark.common.Util.getJsonSchemaFromJsonFile;
import static org.apache.spark.sql.functions.from_json;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.spark.common.SparkConfig;
/*
    This job reads and counts the pixel hits and populates them in mysql
    mango reads the pixel stats to show the pixel counts
 */
public class PixelStatsPopulator {

    private final static Logger LOGGER = LoggerFactory.getLogger(PixelStatsPopulator.class);
    //good documentation on how to read data from cassandra :
    //https://github.com/datastax/spark-cassandra-connector/blob/master/doc/14_data_frames.md
    //https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/sql/JavaSparkSQLExample.java
    private final SparkConfig sparkConfig;

    public PixelStatsPopulator(Map<String, String> arguments) {
        sparkConfig = new SparkConfig(arguments);
    }

    private void createPixelStats() {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "eventlog");
        long lookBackInMillis = Instant.now().minus(
                sparkConfig.getConfigService().getAsInt("job.pixelStatsPopulator.numberOfDaysToBackFill"),
                ChronoUnit.DAYS).toEpochMilli();
        LOGGER.debug("lookBackInMillis : " + lookBackInMillis);
        //you can infer schema from a sample of event log in a file or create the structType yourself
        //the first way is much easier, the content
        //sample-event-log.txt exists in src/main/resources directory, in case it's deleted from /tmp dir
        StructType eventSchema = getJsonSchemaFromJsonFile(sparkConfig.getSparkSession(),
                sparkConfig.getConfigService().get("spark.eventLogFilePath") + "/sample-event-log.txt");
        LOGGER.debug("eventDataSetSample printSchema");

        JavaRDD<Row> tgStatsRDD = dataSet
                .select(new Column("eventtimeinhourpercision"),
                        from_json(new Column("event_in_json"), eventSchema)
                                .getField("pixelId")
                                .as("pixelId"))
                .where("event_type='ACTION'")
                .where("pixelId > 0 ")
                .filter((FilterFunction<Row>) row -> {
                    long rowTime = row.getTimestamp(0).getTime();

                    boolean qualify = row.getTimestamp(0).getTime() >= lookBackInMillis;
                    LOGGER.trace("lookBackInMillis : {}, rowTime : {}, qualify : {}", lookBackInMillis,  rowTime, qualify);

                    return qualify;
                })
                .withColumnRenamed("pixelId", "pixel_id")
                .groupBy("eventtimeinhourpercision", "pixel_id")
                .count()
                .toJavaRDD();

        List<StructField> fields = new ArrayList<>();
        addToFieldsOfSchema(fields, "hit_date", DataTypes.TimestampType);
        addToFieldsOfSchema(fields, "pixel_id", DataTypes.LongType);
        addToFieldsOfSchema(fields, "hits", DataTypes.LongType);


        StructType schema = DataTypes.createStructType(fields);

        LOGGER.info("numberOfPartitions : {}", tgStatsRDD.getNumPartitions());

        Dataset<Row> impRecords = sparkConfig.getSqlContext()
                .createDataFrame(tgStatsRDD, schema);

        List<Row> rows = impRecords.collectAsList();
        LOGGER.debug("created number Of records : {}", rows.size());

        String tableName = "pixel_stats";
        sparkConfig.getMySqlDriver().runSqlCommand(
                "delete from pixel_stats where UNIX_TIMESTAMP(created_at) >= " + lookBackInMillis / 1000);
        sparkConfig.getMySqlDriver().appendDataSetInMysql(tableName, impRecords);
    }

    public static void main(String[] args) {
        LOGGER.info("program arguments, args : {}", Arrays.asList(args));
        Map<String, String> arguments = new HashMap<>();
        Arrays.stream(args).forEach(arg -> {
            String[] keyValue = arg.split("=");
            if (keyValue.length == 2) {
                arguments.put(keyValue[0], keyValue[1]);
            }
        });

        PixelStatsPopulator updater = new PixelStatsPopulator(arguments);
        try {
            updater.createPixelStats();
            updater.sparkConfig.getMetricReporterService().addStateModuleForEntity(
                    "JOB_ENDED_WITH_SUCCESS",
                    updater.getClass().getSimpleName(),
                    "ALL");
        } catch (Exception e) {
            LOGGER.error("exception occurred", e);
            updater.sparkConfig.getMetricReporterService().addStateModuleForEntity(
                    "JOB_ENDED_WITH_FAILURE",
                    updater.getClass().getSimpleName(),
                    "ALL");
        }
        updater.sparkConfig.stopSession();
    }
}
