package com.dstillery.spark.jobs;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import com.dstillery.spark.common.MySqlDriver;

public class CommonUtil implements Serializable {

    public static Set<Long> getCurrentSegmentIds(MySqlDriver mySqlDriver) {
        return mySqlDriver.executeQuery("select id from new_mango.segment", resultSet -> {
            Set<Long> segmentIds = new HashSet<>();

            while(true) {
                try {
                    if (!resultSet.next()) {
                        break;
                    }
                    segmentIds.add(resultSet.getLong(1));
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
            return segmentIds;
        });
    }
}
