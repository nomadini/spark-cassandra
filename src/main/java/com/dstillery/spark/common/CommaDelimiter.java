package com.dstillery.spark.common;

import java.io.Serializable;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

public class CommaDelimiter implements Function<Row, String>, Serializable {
    private static final long serialVersionUID = 7686721187241429609L;

    public String call(Row r) {
        return r.mkString(",");
    }
}
