package com.dstillery.spark.common;

import static java.lang.System.exit;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.codahale.metrics.MetricRegistry;
import com.dstillery.common.config.ConfigService;
import com.dstillery.common.config.GitService;
import com.dstillery.common.metric.InfluxDbClient;
import com.dstillery.common.metric.dropwizard.MetricReporterService;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.scheduler.SparkListener;
import org.apache.spark.scheduler.SparkListenerJobEnd;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spark_project.guava.collect.ImmutableMap;

import scala.Tuple2;

/**
 * Good Documentation
 * https://docs.databricks.com/spark/latest/data-sources/sql-databases.html
 */
public class SparkConfig implements Serializable {
    private final static Logger LOGGER = LoggerFactory.getLogger(SparkConfig.class);
    private final MetricReporterService metricReporterService;
    private SQLContext sqlContext;
    private SparkConf sparkConf;
    private JavaSparkContext javaSparkContext;
    private SparkSession sparkSession;
    private MySqlDriver mySqlDriver;
    private ConfigService configService;

    private InfluxDbClient influxDbClient;

    public SparkConfig() {
        this(ImmutableMap.of());
    }

    public SparkConfig(Map<String, String> arguments) {
        LOGGER.info("argument map : {}", arguments);


        sparkConf = new SparkConf(true);

        configService = new ConfigService(new GitService(),
                sparkConf.get("spark.config.repo.url"),
                sparkConf.get("spark.config.repo.dir"),
                "spark",
                sparkConf.get("spark.application.environment"),
                sparkConf.get("spark.application.cluster"));

        sparkConf
//                .setMaster(configService.get("spark.master.url"))//this causes problems!!!
                .set("spark.executor.extraJavaOptions", configService.get("spark.executor.extraJavaOptions"))
                .set("spark.cassandra.connection.host", configService.get("spark.cassandra.connection.host"))
                .set("spark.hadoop.validateOutputSpecs", "false")
                .set("spark.cassandra.connection.port", configService.get("spark.cassandra.connection.port"))
                .set("spark.cassandra.auth.username", configService.get("spark.cassandra.auth.username"))
                .set("spark.cassandra.auth.password", configService.get("spark.cassandra.auth.password"))
                // This number reflects the approximate amount of Cassandra Data in any given Spark partition.
                // To increase the number of Spark Partitions decrease this number from the default (64mb) to
                // one that will sufficiently break up your Cassandra token range
                .set("spark.cassandra.input.split.size_in_mb", configService.get("spark.cassandra.input.split.size_in_mb"));

        sparkSession = SparkSession
                .builder()
                .config(sparkConf)
                .getOrCreate();

        LOGGER.info("all loaded configurations");
        for(Tuple2<String, String> tuple : sparkSession.sparkContext().getConf().getAll()) {
            LOGGER.info("config : {}={}", tuple._1, tuple._2);
        }

        influxDbClient = new InfluxDbClient(configService.get("influx.host"), configService.get("influx.username"), configService.get("influx.password"));
        metricReporterService = new MetricReporterService(influxDbClient, new MetricRegistry(), "spark_cassandra", 20);

        mySqlDriver = new MySqlDriver(configService.get("spark.mysqlJdbcUrl"),
                configService.get("spark.mysqlUserName"),
                configService.get("spark.mysqlUserPassword"));

        sparkSession.sparkContext().addSparkListener(new SparkListener() {
            @Override
            public void onJobEnd(SparkListenerJobEnd jobEnd) {
                super.onJobEnd(jobEnd);
                //we cannot save entities here because, sometimes it's too early,
                // for example in case of updating in jredis
            }
        });

    }

    public MySqlDriver getMySqlDriver() {
        return mySqlDriver;
    }

    public Dataset<Row> getRowDatasetFromCassandra(
            String keySpace,
            String tableName) {

        Objects.requireNonNull(sparkSession);

        Map<String, String> map = new HashMap<>();
        map.put("keyspace" , keySpace);
        map.put("table" , tableName);

        return sparkSession.read()
                .format("org.apache.spark.sql.cassandra")
                .options(map).load();
    }

    public Dataset<Row> executeQueryOnCassandra(String query) {
        Objects.requireNonNull(sparkSession);
        return sparkSession.sql(query);
    }

    public JavaPairReceiverInputDStream getkafkaStream() {
        /*
         * https://spark.apache.org/docs/latest/streaming-kafka-0-8-integration.html
         */
        String zkCluster = "";
        String consumerGroup = "";
        Integer numberOfPartition = 0;
        JavaStreamingContext javaStreamingContext = null;
        Map<String, Integer> map = new HashMap<>();
        JavaPairReceiverInputDStream<String, String> kafkaStream =
                KafkaUtils.createStream(javaStreamingContext,
                        zkCluster, consumerGroup, map);
        return kafkaStream;
    }

    public SparkContext getSparkContext() {
        return sparkSession.sparkContext();
    }

    public SQLContext getSqlContext() {
        if (sqlContext == null) {
            sqlContext = new SQLContext(getSparkContext());
        }
        return sqlContext;
    }

    public SparkConf getSparkConf() {
        return sparkConf;
    }

    public ConfigService getConfigService() {
        return configService;
    }

    @Deprecated
    public JavaSparkContext getJavaSparkContext() {
        if (javaSparkContext == null) {
            javaSparkContext = new JavaSparkContext(sparkSession.sparkContext());
        }
        return javaSparkContext;
    }

    public SparkSession getSparkSession() {
        return sparkSession;
    }

    public void stopSession() {
        LOGGER.info("Job is done. persisting entity states in influx now");
        // sleep for 10 millis so all states gets written to the queue inside metricReporterService object
        try {
            Thread.sleep(10);
            metricReporterService.persistStats();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            sparkSession.stop();
            sparkSession.close();
            LOGGER.info("Job is done. Exiting manually. Bye!");
            exit(0);
        }
    }

    public MetricReporterService getMetricReporterService() {
        return metricReporterService;
    }
}
