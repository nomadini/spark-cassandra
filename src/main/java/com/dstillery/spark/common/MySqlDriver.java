package com.dstillery.spark.common;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Function;

import org.apache.spark.sql.DataFrameWriter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MySqlDriver implements Serializable {
    private final static Logger LOGGER = LoggerFactory.getLogger(MySqlDriver.class);

    private String jdbcUrl;
    private String mysqlUserName;
    private String mysqlPassword;

    public MySqlDriver(String jdbcUrl, String mysqlUserName, String mysqlPassword) {
        Objects.requireNonNull(jdbcUrl);
        Objects.requireNonNull(mysqlUserName);
        Objects.requireNonNull(mysqlPassword);
        this.jdbcUrl = jdbcUrl;
        this.mysqlUserName = mysqlUserName;
        this.mysqlPassword = mysqlPassword;
    }

    public void runSqlCommand(String sqlStatement) {
        try {
            Connection conn = null;
            Statement stmt = null;
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(jdbcUrl,
                    mysqlUserName,
                    mysqlPassword);
            stmt = conn.createStatement();
            stmt.executeUpdate(sqlStatement);
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public <T> T executeQuery(String sqlStatement, Function<ResultSet, T> resultSetProcessor) {
        try {
            Connection conn = null;
            Statement stmt = null;
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(jdbcUrl,
                    mysqlUserName,
                    mysqlPassword);
            stmt = conn.createStatement();
            ResultSet resultSet = stmt.executeQuery(sqlStatement);
            T result = resultSetProcessor.apply(resultSet);
            stmt.close();
            conn.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveDataWithMode(String tableName,
                                 Dataset<Row> tgStatsDF,
                                 SaveMode saveMode) {

        Properties connectionProperties = new Properties();
        connectionProperties.put("driver", "com.mysql.jdbc.Driver");
        connectionProperties.put("user", mysqlUserName);
        connectionProperties.put("password", mysqlPassword);
        DataFrameWriter writer = new DataFrameWriter<>(tgStatsDF);

        LOGGER.info("saving info in table : {}", tableName);

        writer.mode(saveMode).
                jdbc(jdbcUrl,
                     tableName,
                     connectionProperties);
        writer.save();
    }

    public void appendDataSetInMysql(String tableName, Dataset<Row> tgStatsDF) {
        saveDataWithMode(tableName, tgStatsDF, SaveMode.Append);
    }

    public void overWrite(String tableName, Dataset<Row> tgStatsDF) {
        saveDataWithMode(tableName, tgStatsDF, SaveMode.Overwrite);
    }

}
