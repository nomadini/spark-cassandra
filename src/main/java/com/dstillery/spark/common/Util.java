package com.dstillery.spark.common;

import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spark_project.guava.collect.ImmutableList;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.dstillery.common.util.GUtil;

public class Util {
    private final static Logger LOGGER = LoggerFactory.getLogger(Util.class);

    public static void saveRDDInFile(JavaRDD<Row> countsRDD, String folderName) {
        countsRDD.map(new CommaDelimiter())
                .coalesce(1, true)
                .saveAsTextFile(folderName);
    }

    public static void logRDD(JavaRDD<Row> rows) {

        rows.foreach(row -> {
            LOGGER.info("row : {}", row.toString());
        });
    }
    public static void logRDD(Row row) {
        LOGGER.info("row : {}", row.toString());
        LOGGER.info("row schema: {}", row.schema());
    }

    public static void printDataSet(Dataset<Row> set) {
        List<Row> listOfHotFeatures = set.collectAsList();
        listOfHotFeatures.stream().forEach((row) -> {
            LOGGER.debug("row : {}", row.mkString());
            LOGGER.debug("row schema {} : ", row.schema());
        });
    }

    public static void runSqlCommandViaSpark(String sqlStatement, SparkConfig config) {
        config.getSqlContext().sql(sqlStatement);
    }


    public static void saveStringRDDInFile(JavaRDD<String> countsRDD, String folderName) {
        countsRDD
                .coalesce(1, true)
                .saveAsTextFile(folderName);
    }

    public static <T> JavaRDD<T> convertListToRDD(SparkConfig sparkConfig, List<T> rdd) {
        return sparkConfig.getJavaSparkContext().parallelize(rdd);
    }

    public static JavaRDD<String> convertObjectToRDD(SparkConfig sparkConfig, String rdd) {
        return sparkConfig.getJavaSparkContext().parallelize(ImmutableList.of(rdd));
    }

    public static void saveListInFolder(List<String> histogramArray,
                                        String folderName,
                                        SparkConfig sparkConfig) {
        convertListToRDD(sparkConfig, histogramArray)
                .coalesce(1, true)
                .saveAsTextFile(folderName);
    }

    public static void addToFieldsOfSchema(List<StructField> fields, String fieldName, DataType dataType) {
        StructField field = DataTypes.createStructField(fieldName, dataType, true);
        fields.add(field);
    }

    public static ResultSet executeCassandraCommand(Session session, String sql) {
        int retry = 0;
        while(retry < 4) {
            try {
                return session.execute(sql);
            } catch (Exception e) {
                LOGGER.warn("suppressing exception. retrying in 5 seconds", e);
                GUtil.sleepInSeconds(5);
            } finally {
                retry++;
            }
        }
        LOGGER.warn("returning null, failure after 5 tries");
        return null;
    }

    public static void saveDataSetToFile(Dataset<Row> dataSet, String folderName) {
        dataSet//The coalesce method reduces the number of partitions in a DataFrame. Here’s how to consolidate the data in 1 partitions:
                .coalesce(1)//this makes spark to write to a single file
                .write()
                .mode(SaveMode.Overwrite)
                .format("com.databricks.spark.csv").save(folderName);
    }

    public static StructType getJsonSchemaFromJsonFile(SparkSession sparkSession, String pathToFile) {
        return sparkSession.read().json(pathToFile).schema();
    }
}


