package prototyping;

import static com.dstillery.spark.common.Util.addToFieldsOfSchema;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.spark.common.SparkConfig;

public class ImpressionCompressor {

    private final static Logger LOGGER = LoggerFactory.getLogger(ImpressionCompressor.class);
    //good documentation on how to read data from cassandra :
    //https://github.com/datastax/spark-cassandra-connector/blob/master/doc/14_data_frames.md
    //https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/sql/JavaSparkSQLExample.java
    private final SparkConfig sparkConfig;

    public ImpressionCompressor() {

        sparkConfig = new SparkConfig();
    }

    public static void main(String[] args) throws Exception {

        ImpressionCompressor updater = new ImpressionCompressor();
        updater.compressImpression();

//        notWorkingExample();
    }

    private void compressImpression() {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "eventlog");
        long beginningOfTodayInMillis = Instant.now().truncatedTo(ChronoUnit.HOURS).toEpochMilli();
        LOGGER.debug("beginningOfTodayInMillis : " + beginningOfTodayInMillis);

        JavaRDD<Row> tgStatsRDD = dataSet
                .select(
                        "eventtimeinhourpercision",
                        "client_id",
                        "advertiser_id",
                        "campaign_id",
                        "targetgroup_id",
                        "creative_id",
                        "geosegmentlist_id",
                        "adSize",
                        "deviceCountry",
                        "deviceState",
                        "deviceZipCode",
                        "impressionType",
                        "deviceType",
                        "siteCategory",
                        "siteDomain",
                        "event_type",
                        "count",
                        "advertiserCostInCpm",
                        "bidPriceInCpm",
                        "exchangeCostInCpm",
                        "platformCostInCpm"
                )
                .where("event_type='impression'")
                .where("scrub_type=410")
                .filter((FilterFunction<Row>) row -> {
//                    Date date = formatter.parse(row.getString(0));
                    long rowTime = row.getTimestamp(0).getTime();

                     boolean qualify = row.getTimestamp(0).getTime() >= beginningOfTodayInMillis;
                    LOGGER.debug("beginningOfTodayInMillis : {}, rowTime : {}, qualify : {}",
                                    beginningOfTodayInMillis,  rowTime, qualify);
                    return qualify;
                })
                .groupBy(
                        "eventtimeinhourpercision",
                        "client_id",
                        "advertiser_id",
                        "campaign_id",
                        "targetgroup_id",
                        "creative_id",
                        "geosegmentlist_id",
                        "adSize",
                        "deviceCountry",
                        "deviceState",
                        "deviceZipCode",
                        "impressionType",
                        "deviceType",
                        "siteCategory",
                        "siteDomain",
                        "event_type"
                        )
                .sum(   "count",
                        "advertiserCostInCpm",
                        "bidPriceInCpm",
                        "exchangeCostInCpm",
                        "platformCostInCpm")
                .toJavaRDD();

        List<StructField> fields = new ArrayList<>();
        addToFieldsOfSchema(fields, "created_at", DataTypes.TimestampType);
        addToFieldsOfSchema(fields, "client_id", DataTypes.LongType);
        addToFieldsOfSchema(fields, "advertiser_id", DataTypes.LongType);
        addToFieldsOfSchema(fields, "campaign_id", DataTypes.LongType);
        addToFieldsOfSchema(fields, "targetgroup_id", DataTypes.LongType);
        addToFieldsOfSchema(fields, "creative_id", DataTypes.LongType);
        addToFieldsOfSchema(fields, "geosegmentlist_id", DataTypes.LongType);
        addToFieldsOfSchema(fields, "adSize", DataTypes.StringType);
        addToFieldsOfSchema(fields, "deviceCountry", DataTypes.StringType);
        addToFieldsOfSchema(fields, "deviceState", DataTypes.StringType);
        addToFieldsOfSchema(fields, "deviceZipCode", DataTypes.StringType);
        addToFieldsOfSchema(fields, "impressionType", DataTypes.StringType);
        addToFieldsOfSchema(fields, "deviceType", DataTypes.StringType);
        addToFieldsOfSchema(fields, "siteCategory", DataTypes.StringType);
        addToFieldsOfSchema(fields, "siteDomain", DataTypes.StringType);
        addToFieldsOfSchema(fields, "event_type", DataTypes.StringType);
        addToFieldsOfSchema(fields, "count", DataTypes.LongType);
        addToFieldsOfSchema(fields, "advertiserCostInCpm", DataTypes.DoubleType);
        addToFieldsOfSchema(fields, "bidPriceInCpm", DataTypes.DoubleType);
        addToFieldsOfSchema(fields, "exchangeCostInCpm", DataTypes.DoubleType);
        addToFieldsOfSchema(fields, "platformCostInCpm", DataTypes.DoubleType);

        StructType schema = DataTypes.createStructType(fields);

        LOGGER.info("numberOfPartitions : {}", tgStatsRDD.getNumPartitions());

        Dataset<Row> impRecords = sparkConfig.getSqlContext()
                .createDataFrame(tgStatsRDD, schema);

        List<Row> rows = impRecords.collectAsList();
        LOGGER.debug("created number Of records : {}", rows.size());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
        Date date = new Date();
        String tableName = "impression_compressed_"+ dateFormat.format(date);
        sparkConfig.getMySqlDriver().overWrite(tableName, impRecords);
    }

}
