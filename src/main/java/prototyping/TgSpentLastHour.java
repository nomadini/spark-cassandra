package prototyping;

import java.io.Serializable;

public class TgSpentLastHour implements Serializable{
    private int targetGroupId;
    private double platformBudgetSpentInHour;

    public int getTargetGroupId() {
        return targetGroupId;
    }

    public void setTargetGroupId(int targetGroupId) {
        this.targetGroupId = targetGroupId;
    }

    public double getPlatformBudgetSpentInHour() {
        return platformBudgetSpentInHour;
    }

    public void setPlatformBudgetSpentInHour(double platformBudgetSpentInHour) {
        this.platformBudgetSpentInHour = platformBudgetSpentInHour;
    }
}
