package prototyping;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.spark.common.SparkConfig;

public class TopDeviceSeenInStream {

    private final static Logger LOGGER = LoggerFactory.getLogger(TopDeviceSeenInStream.class);
    //good documentation on how to read data from cassandra :
    //https://github.com/datastax/spark-cassandra-connector/blob/master/doc/14_data_frames.md
    //https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/sql/JavaSparkSQLExample.java
    private final SparkConfig sparkConfig;

    public TopDeviceSeenInStream() {
        sparkConfig = new SparkConfig();
    }

    public static void main(String[] args) throws Exception {

        TopDeviceSeenInStream updater = new TopDeviceSeenInStream();
        updater.findCommonDevicesInLastWeek();
    }

    private void findCommonDevicesInLastWeek() {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "bideventlog");
        long last7Days = Instant.now().toEpochMilli() - (7 * 24 * 3600 * 1000);
        LOGGER.debug("last7Days : " + last7Days);

        JavaRDD<Row> countsRDD = dataSet
                .select("eventtime",
                        "nomadinideviceid",
                        "count"
                )
                .filter((FilterFunction<Row>) row -> {
                    long rowTime = row.getTimestamp(0).getTime();
                    boolean qualify = row.getTimestamp(0).getTime() >= last7Days;
                    LOGGER.debug("last7Days : {}, rowTime : {}, qualify : {}",
                            last7Days,  rowTime, qualify);
                    return qualify;
                })
                .groupBy("nomadinideviceid")
                .sum("count")
                //first we need to sum the counts and then filter
                .filter((FilterFunction<Row>) row -> {
                    long count = row.getLong(1);
                    boolean qualify = count > 2;
                    return qualify;
                })
                .sort("sum(count)")
                .toJavaRDD();


        List<StructField> fields = new ArrayList<>();
        addToFieldsOfSchema(fields, "nomadinideviceid", DataTypes.StringType);
        addToFieldsOfSchema(fields, "count", DataTypes.LongType);

        StructType schema = DataTypes.createStructType(fields);

        LOGGER.info("numberOfPartitions : {}", countsRDD.getNumPartitions());

        Dataset<Row> tgStatsDF = sparkConfig.getSqlContext()
                .createDataFrame(countsRDD, schema);

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
        String fileName = "common_devices_"+ dateFormat.format(date);
        LOGGER.info("saving info in file : {}", fileName);
        tgStatsDF
                //The coalesce method reduces the number of partitions in a DataFrame. Here’s how to consolidate the data in 1 partitions:
                .coalesce(1)//this makes spark to write to a single file
                .write()
                .mode(SaveMode.Overwrite)
                .format("com.databricks.spark.csv").save("/tmp/"+ fileName);

    }

    private void addToFieldsOfSchema(List<StructField> fields, String fieldName, DataType dataType) {
        StructField field = DataTypes.createStructField(fieldName, dataType, true);
        fields.add(field);
    }
}
