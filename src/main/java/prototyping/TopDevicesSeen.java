package prototyping;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapToRow;
import static com.dstillery.spark.common.Util.convertListToRDD;
import static com.dstillery.spark.common.Util.saveListInFolder;
import static com.dstillery.spark.common.Util.saveRDDInFile;

import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.DoubleFunction;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.spark.connector.BytesInBatch;
import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.dstillery.spark.common.SparkConfig;
import com.dstillery.spark.jobs.models.BadDevice;

import scala.Tuple2;

public class TopDevicesSeen {

    private final static Logger LOGGER = LoggerFactory.getLogger(TopDevicesSeen.class);
    //good documentation on how to read data from cassandra :
    //https://github.com/datastax/spark-cassandra-connector/blob/master/doc/14_data_frames.md
    //https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/sql/JavaSparkSQLExample.java
    private final SparkConfig sparkConfig;

    public TopDevicesSeen() {
        sparkConfig = new SparkConfig();
    }

    public static void main(String[] args) throws Exception {

        TopDevicesSeen updater = new TopDevicesSeen();
        updater.findCommonDevicesInLastWeek();

    }

    private void findCommonDevicesInLastWeek() {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "bideventlog");
        long last7Days = Instant.now().minus(7, ChronoUnit.DAYS).toEpochMilli();
        LOGGER.debug("last7Days : " + last7Days);

        JavaRDD<Row> countsRDD = dataSet
                .select("eventtime",
                        "nomadinideviceid",
                        "count"
                )
                .filter((FilterFunction<Row>) row -> {
                    long rowTime = row.getTimestamp(0).getTime();
                    boolean qualify = row.getTimestamp(0).getTime() >= last7Days;
                    LOGGER.debug("last7Days : {}, rowTime : {}, qualify : {}",
                            last7Days,  rowTime, qualify);
                    return qualify;
                })
                .groupBy("nomadinideviceid")
                .sum("count")
                //first we need to sum the counts and then filter
                .filter((FilterFunction<Row>) row -> {
                    long count = row.getLong(1);
                    boolean qualify = count > 10;
                    return qualify;
                })
                .sort("sum(count)")
                .toJavaRDD();


        LOGGER.info("numberOfPartitions : {}", countsRDD.getNumPartitions());

        JavaRDD<Row> top30Rdd = convertListToRDD(sparkConfig,
                countsRDD.takeOrdered(30, new TopDevicesRowDescendingComparator()));
        String folderName = "/tmp/top_common_devices";

        saveRDDInFile(top30Rdd, folderName);

        JavaDoubleRDD countsAsDoubles = countsRDD.mapToDouble(
                (DoubleFunction<Row>) x -> x.getLong(1));

        saveHistogramInFolder(countsAsDoubles, 5);
        Instant thisHour = Instant.now().truncatedTo(ChronoUnit.HOURS);
        saveRddToCassandra(top30Rdd.map(new BadDeviceMapper(thisHour)));
    }

    private void saveRddToCassandra(JavaRDD<BadDevice> top30Rdd) {

        Map<String, String> fieldToColumnMapping = new HashMap<>();
        fieldToColumnMapping.put("deviceId", "deviceid");
        fieldToColumnMapping.put("count", "numberoftimesseen");
        fieldToColumnMapping.put("time", "eventtime");

        CassandraJavaUtil.javaFunctions(top30Rdd)
                .writerBuilder(
                        "analytics", "baddeviceslist",
                        mapToRow(BadDevice.class, fieldToColumnMapping))
                .withConstantTTL(Duration.standardDays(7))
                .withBatchSize(new BytesInBatch(1000))
                .saveToCassandra();
    }

    private void saveHistogramInFolder(JavaDoubleRDD countsRDD, int bucketCount) {
        String folderName = "/tmp/histogram_common_devices";
        List<String> histogramArray = new ArrayList<>();
        Tuple2<double[], long[]> histogram = countsRDD.histogram(bucketCount);

        for (int i = 0; i <= histogram._1.length - 1; i++) {
            double bucketValue = histogram._1[i];
            long countOfBuck = histogram._2[i];
            String row = bucketValue + ":" + countOfBuck;
            histogramArray.add(row);
        }

        saveListInFolder(histogramArray, folderName, sparkConfig);

    }

    private void addToFieldsOfSchema(List<StructField> fields, String fieldName, DataType dataType) {
        StructField field = DataTypes.createStructField(fieldName, dataType, true);
        fields.add(field);
    }
}

/**
 * shouldn't be inner class, because its serialize, if not, outer class has to become serializable
 */
class TopDevicesRowDescendingComparator implements Comparator<Row>, Serializable {
    @Override
    public int compare(Row o1, Row o2) {
        if (o1.getLong(1) == o2.getLong(1)) {
            return 0;
        }
        if (o1.getLong(1) > o2.getLong(1)) {
            return -1;
        }
        return 1;
    }
};

/**
 * shouldn't be inner class
 */
class CommaDelimiter implements Function<Row, String>, Serializable {

    public String call(Row r) {
        return r.mkString(",");
    }
}

class BadDeviceMapper implements Function<Row, BadDevice>, Serializable {
    private Instant instant;

    public BadDeviceMapper(Instant instant) {
        this.instant = instant;
    }

    @Override
    public BadDevice call(Row row) throws Exception {
        return new BadDevice(row.getString(0), row.getLong(1), Date.from(instant));
    }
}


