package prototyping;

import static com.dstillery.spark.common.Util.convertObjectToRDD;
import static com.dstillery.spark.common.Util.saveStringRDDInFile;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.DoubleFunction;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dstillery.spark.common.SparkConfig;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import scala.Tuple2;

public class ModelScoreDistributionCalculator {

    private final static Logger LOGGER = LoggerFactory.getLogger(ModelScoreDistributionCalculator.class);
    //good documentation on how to read data from cassandra :
    //https://github.com/datastax/spark-cassandra-connector/blob/master/doc/14_data_frames.md
    //https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/sql/JavaSparkSQLExample.java
    private final SparkConfig sparkConfig;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public ModelScoreDistributionCalculator() {
        sparkConfig = new SparkConfig();
    }

    public static void main(String[] args) throws Exception {

        ModelScoreDistributionCalculator topDeviceAndIpAndMrgs10SeenProducer = new ModelScoreDistributionCalculator();
        topDeviceAndIpAndMrgs10SeenProducer.saveModelDistributions(168, "/tmp/model_score_distribution_last7days");
    }

    private void saveModelDistributions(int hoursAgo, String folderName) throws JsonProcessingException {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("gicapods", "modelscore");
        long lastNHours = Instant.now().minus(hoursAgo, ChronoUnit.HOURS).toEpochMilli();
        LOGGER.info("finding top data for : folderName : {}, lastNHours : {}", folderName, lastNHours);

        Dataset<Row> allScores = dataSet
                .select("eventtime",
                        "modelid",
                        "score"
                )
                .filter((FilterFunction<Row>) row -> row.getTimestamp(0).getTime() >= lastNHours);

        LOGGER.info("scores count: {}", allScores.count());
        JavaRDD<Row> allModelIdsRDD = allScores.select("modelid").distinct().toJavaRDD();


        HashMap<Integer, List<Double>> modelIdDistributions = new HashMap<>();


        List<Integer> modelIds = allModelIdsRDD.map(row -> row.getInt(0)).collect();

        modelIds.forEach(modelId -> {
            JavaRDD<Row> allScoresForModel = dataSet
            .select("eventtime",
                    "modelid",
                    "score"
            ).where("modelId="+modelId)
             .filter((FilterFunction<Row>) innerRow -> innerRow.getTimestamp(0).getTime() >= lastNHours).toJavaRDD();

            if (allScoresForModel.count() <= 0) {
                LOGGER.info("aborting. haven't found busy data");
                return;
            }

            JavaDoubleRDD countsAsDoubles = allScoresForModel.mapToDouble(x -> x.getDouble(2));

            List<Double> allBucketValues = getAllStartBucketValues(countsAsDoubles, 8);
            modelIdDistributions.put(modelId, allBucketValues);
        });

        String modelJson = objectMapper.writeValueAsString(modelIdDistributions);

        LOGGER.info("all model distros : {}", modelJson);
        saveStringRDDInFile(convertObjectToRDD(sparkConfig, modelJson), folderName);
    }

    private  List<Double> getAllStartBucketValues(JavaDoubleRDD countsRDD, int bucketCount) {
        List<Double> allBucketValues = new ArrayList<>();
        Tuple2<double[], long[]> histogram = countsRDD.histogram(bucketCount);
        for (int i = 0; i <= histogram._1.length - 1; i++) {
            double bucketValue = histogram._1[i];
            allBucketValues.add(bucketValue);
        }
        return allBucketValues;
    }
}
