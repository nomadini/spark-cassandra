package prototyping;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import com.dstillery.spark.common.SparkConfig;

public class ImpsByAdSizeAndStateCounter {
    private SparkConfig sparkConfig;

    public ImpsByAdSizeAndStateCounter() {
        sparkConfig = new SparkConfig();
    }

    public static void main(String[] args) throws Exception {
        ImpsByAdSizeAndStateCounter updater = new ImpsByAdSizeAndStateCounter();
        updater.count();
    }

    private void count() {
        Dataset<Row> dataSet = sparkConfig.getRowDatasetFromCassandra("eventlog", "gicapods");
        dataSet.groupBy("eventType", "adSize", "usertimezone").sum("winbidprice")
                .collectAsList().stream().forEach(row -> {
            System.out.println("row : " + row.mkString());
        });
    }
}
