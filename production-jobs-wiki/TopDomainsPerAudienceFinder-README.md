

## Description of PixelStatsPopulator Job 

* PixelStatsPopulator :  This job reads and counts the pixel hits and populates them in mysql
mango reads the pixel stats to show the pixel counts  

### how to run it on mac? 
     order of properties are very important
     
    cd /Users/mtaabodi/Documents/workspace-mt/spark-cassandra/ \
    && mmvn clean install && spark-submit --verbose  --class com.dstillery.spark.jobs.topdomains_finder.TopDomainsPerAudienceFinder  \
    --conf "spark.config.repo.url=git@bitbucket.org:nomadini/config.git" \
    --conf "spark.config.repo.dir=/Users/mtaabodi/Documents/workspace-mt/config" \
    --conf "spark.application.environment=local" \
    --conf "spark.application.cluster=default" \
    --conf "spark.debug.maxToStringFields=10000" \
    --conf "spark.master.url=spark://localhost:7077" \
    --master local[8]  /Users/mtaabodi/Documents/workspace-mt/spark-cassandra/target/spark-cassandra-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar 100

 how to enable logging in spark on mac:
cp /usr/local/spark/conf/log4j.properties.template /usr/local/spark/conf/log4j.properties
add your logging level here
      log4j.logger.com.dstillery=DEBUG
       vi /usr/local/spark/conf/log4j.properties


### how to run it on linux?
