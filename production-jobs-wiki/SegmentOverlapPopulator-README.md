

## Description of SegmentOverlapPopulator Job 

   This job reads and counts the overlapped counts of all possible segment pair combination and writes them
    to analytics.segment_overlap table in cassandra, the source of data is gicapods.daily_unique_device_bid_eventlog

### how to run it on mac? 
     order of properties are very important
     
    cd /Users/mtaabodi/Documents/workspace-mt/spark-cassandra/ \
    && mmvn clean install && spark-submit --verbose  --class com.dstillery.spark.jobs.segment_overlap.SegmentOverlapPopulator  \
    --conf "spark.config.repo.url=git@bitbucket.org:nomadini/config.git" \
    --conf "spark.config.repo.dir=/Users/mtaabodi/Documents/workspace-mt/config" \
    --conf "spark.application.environment=local" \
    --conf "spark.application.cluster=default" \
    --conf "spark.master.url=spark://localhost:7077" \
    --master local[8]  /Users/mtaabodi/Documents/workspace-mt/spark-cassandra/target/spark-cassandra-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar 100

 

 how to enable logging in spark on mac:
cp /usr/local/spark/conf/log4j.properties.template /usr/local/spark/conf/log4j.properties
add your logging level here
      log4j.logger.com.dstillery=DEBUG
       vi /usr/local/spark/conf/log4j.properties


### how to run it on linux?
