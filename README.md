# Spark Cassandra Project

## Description of Jobs

READ all the job readmes in production-jobs-wiki directory 

* TopDeviceAndIpAndMrgs10SeenProducer : finds the top deviceIds and Ipds and mgrs10 in the past week and 
writes them to a file that is read by the bidder, which is considered bad devices and bad ips
* PixelStatsPopulator :  This job reads and counts the pixel hits and populates them in mysql
mango reads the pixel stats to show the pixel counts  

* TopMGRS100FinderPerIabCategory : finds the topMGrs100 palaces for each iab category and writes it to redis
UI uses this info in GeoAnalysis Controller

## How to setup spark ?
1. first install scala version 2.11.x
sudo apt-get update && sudo apt-get -f install && sudo apt-get remove scala && sudo apt-get install scala && scala -version

2. install maven
  sudo apt-get install maven

3. cd /tmp && wget https://www.apache.org/dyn/closer.lua/spark/spark-2.3.2/spark-2.3.2-bin-hadoop2.7.tgz && tar -xvf spark-2.3.2-bin-hadoop2.7.tgz && mv spark-2.1.0-bin-hadoop2.7  /usr/local/spark

4. add the path and verify spark works
PATH=$PATH:/usr/local/spark/bin --> add this line to your bash and source ~/.bash_profile
verify spark is working using command :  spark-shell

4-1.copy spark env and set the worker params as below, to clean up temp directories
cp /usr/local/spark/conf/spark-env.sh.template /usr/local/spark/conf/spark-env.sh
echo 'SPARK_WORKER_OPTS="-Dspark.worker.cleanup.enabled=true -Dspark.worker.cleanup.interval=120 -Dspark.worker.cleanup.appDataTtl=120"' >>  /usr/local/spark/conf/spark-env.sh

4-2. how to enable logging in sprk:
cp /usr/local/spark/conf/log4j.properties.template /usr/local/spark/conf/log4j.properties
add your logging level here
      log4j.logger.com.dstillery=DEBUG
       vi /usr/local/spark/conf/log4j.properties


5. start the master and worker
how to run cluster : first start master /usr/local/spark/sbin/start-master.sh
//find the address of master from spark logs in /usr/local/spark/logs/ where it says  --host XXXXX
how to run cluster : find the master address from the logs of start-master output
first start one worker /usr/local/spark/sbin/start-slave.sh spark://alpha2.nyc:7077
/usr/local/spark/sbin/start-slave.sh spark://alpha2.nyc:7077


6.1 SPARK UI on Mac is here : http://10.15.103.7:4040/jobs/
7. build your project and submit it to spark
    How to run apache-cassandra app ? answer : build the jar and submit it to spark
git clone https://mtaabodi@bitbucket.org/mtaabodi/spark-cassandra.git && cd spark-cassandra/ && git checkout feature/version-2.1.0-hadoop2.7 && git pull &&  mvn install

thats how you submit a job to spark :
go to r2 server and run the job
spark-submit \
  --class com.dstillery.spark.jobs.TopMGRS100FinderPerIabCategory \
  --master local[8] \
  /home/vagrant/workspace/spark-cassandra/target/spark-cassandra-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar \ 100

spark-submit \
  --class com.dstillery.spark.jobs.PixelStatsPopulator \
  --master local[8] \
  /home/vagrant/workspace/spark-cassandra/target/spark-cassandra-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar \ 100

spark-submit \
  --class com.dstillery.spark.jobs.SegmentCountPopulator \
  --master local[8] \
  /home/vagrant/workspace/spark-cassandra/target/spark-cassandra-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar \ 100


spark sql guide : http://spark.apache.org/docs/latest/sql-programming-guide.html
spark cassandra guide : https://github.com/datastax/spark-cassandra-connector/blob/master/doc/3_selection.md
these are the funcitons you can call on rdd : http://spark.apache.org/docs/latest/programming-guide.html
how to get spark version :  spark-submit --versionHow to run apache-cassandra app ? answer : build the jar and submit it to spark
cd workspace/apache-cassandra
mvn clean install
